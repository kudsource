#include <stdio.h>

#define maxn 1100

int min(int p, int q, int r)
{
	p=(p<q?p:q);
	p=(p<r?p:r);
	return p;
}

int max(int p, int q) 
{
	return p>q?p:q;
}

int f[maxn][maxn],a[maxn][maxn];

int main() 
{
	int n,m,i,j;
	scanf("%d%d",&n,&m);
	for (i=1; i<=n; i++) 
		for (j=1; j<=m; j++) 
			scanf("%d",&(a[i][j]));
	for (i=n; i>0; i--) 
		for (j=m; j>0; j--)
			if (a[i][j]) 
				f[i][j]=min(f[i+1][j],f[i][j+1],f[i+1][j+1])+1;
	int ans=f[1][1];
	for (i=1; i<=n; i++)
		for (j=1; j<=m; j++)
			ans=max(ans,f[i][j]);
	printf("%d\n",ans);
	return 0;
}