#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define maxn 110
#define maxw 100010

int main() {
	int n,total_w;
	scanf("%d%d",&total_w,&n);
	int i,j,w[maxn];
	long long path[maxw],f[maxw];
	for (i=1; i<=n; i++) scanf("%d",&(w[i]));
	memset(f,0,sizeof(f));
	f[0]=1;
	for (i=1; i<=n; i++) 
		for (j=total_w; j>=w[i]; j--)
			if (f[j-w[i]]) {
				if (f[j]==0) path[j]=i;
				f[j]+=f[j-w[i]];
			}
	if (f[total_w]>1) printf("-1\n"); else
		if (f[total_w]==0) printf("0\n"); else {
			bool ans[maxn];
			memset(ans,false,sizeof(ans));
			int tmp=total_w;
			while (tmp) {
				ans[path[tmp]]=true;
				tmp-=w[path[tmp]];
			}
			for (i=1; i<=n; i++) 
				if (!ans[i]) printf("%d ",i);
			printf("\n");
		}
	return 0;
}