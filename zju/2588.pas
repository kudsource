{$inline on}
{$M 10000000}

const
  maxn=10010;
  maxm=100010;

var
  t,src,dest,i,n,m,cnt,ans:longint;
  edge:array [0..maxm shl 1] of record
                            adj,next,id:longint;
                            flag:boolean;
                          end;
  ehead,dep,low,father,mark:array [0..maxn] of longint;
  bridge:array [0..maxm] of boolean;
  tmp:boolean;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

procedure insedge(p,q,k:longint);
var
  i:longint;

begin
  inc(cnt);
  edge[cnt].adj:=q;
  i:=ehead[p];
  while (i>0) and (edge[i].adj<>q) do i:=edge[i].next;
  if i>0 then
    begin
      edge[i].flag:=true;
      exit;
    end;
  edge[cnt].next:=ehead[p];
  edge[cnt].id:=k;
  edge[cnt].flag:=false;
  ehead[p]:=cnt;
end;

procedure dfs(k:longint);
var
  p,q:longint;

begin
  mark[k]:=1;
  low[k]:=dep[k];
  p:=ehead[k];
  while p>0 do
    begin
      q:=edge[p].adj;
      if (q<>father[k]) and (mark[q]=1) then low[k]:=min(low[k],dep[q]);
      if mark[q]=0 then
        begin
          dep[q]:=dep[k]+1;
          father[q]:=k;
          dfs(q);
          low[k]:=min(low[k],low[q]);
          if (low[q]>dep[k]) and (not edge[p].flag) then bridge[edge[p].id]:=true;
        end;
      p:=edge[p].next;
    end;
  mark[k]:=2;
end;

begin
  readln(t);
  repeat
    dec(t);
    readln;
    readln(n,m);
    cnt:=0;
    fillchar(ehead,sizeof(ehead),0);
    fillchar(edge,sizeof(edge),0);
    for i:=1 to m do
      begin
        readln(src,dest);
        insedge(src,dest,i);
        insedge(dest,src,i);
      end;
    fillchar(mark,sizeof(mark),0);
    fillchar(bridge,sizeof(bridge),false);
    fillchar(dep,sizeof(dep),0);
    fillchar(low,sizeof(low),0);
    fillchar(father,sizeof(father),0);
    dep[1]:=1;
    dfs(1);
    ans:=0;
    for i:=1 to m do inc(ans,ord(bridge[i]));
    writeln(ans);
    tmp:=ans>0;
    for i:=1 to m do
      if bridge[i] then
        begin
          dec(ans);
          write(i);
          if ans>0 then write(' ');
        end;
    if tmp then writeln;
    if t>0 then writeln;
  until t=0;
end.
