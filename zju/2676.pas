uses math;

const
  eps=0.1;
  maxm=2000+10;
  maxn=200+10;

var
  edge:array [0..maxm] of record
                            adj,next:longint;
                            cost:double;
                          end;
  n,m,i,cnt,ans,panic:longint;
  l,r,mid,res:double;
  ehead,dep,a:array [0..maxn] of longint;
  data:array [0..maxm] of record
                            src,dest,cost:longint;
                          end;

procedure insert_edge(p,q:longint; c:double);
begin
  edge[cnt].adj:=q;
  edge[cnt].next:=ehead[p];
  edge[cnt].cost:=c;
  ehead[p]:=cnt;
  inc(cnt);
end;

function init(x:double):double;
var
  i:longint;
  tmp:double;

begin
  cnt:=0;
  tmp:=0;
  fillchar(ehead,sizeof(ehead),255);
  for i:=1 to m do
    with data[i] do
      if cost<=x then tmp:=tmp+cost-x else
        begin
          insert_edge(src,dest,cost-x);
          insert_edge(dest,src,cost-x);
        end;
  exit(tmp);
end;

function bfs(s,t:longint):boolean;
var
  queue:array [0..maxn] of longint;
  head,tail,i,j,p:longint;
  w:double;

begin
  fillchar(dep,sizeof(dep),255);
  head:=0;
  tail:=1;
  queue[tail]:=s;
  dep[s]:=0;
  repeat
    inc(head);
    i:=queue[head];
    p:=ehead[i];
    while p<>-1 do
      begin
        j:=edge[p].adj;
        w:=edge[p].cost;
        if (w>0) and (dep[j]<0) then
          begin
            inc(tail);
            queue[tail]:=j;
            dep[j]:=dep[i]+1;
            if j=t then exit(true);
          end;
        p:=edge[p].next;
      end;
  until head>=tail;
  exit(false);
end;

function dfs(now:longint; low:double):double;
var
  tmp,w,sum:double;
  i,p:longint;

begin
  if now=n then exit(low);
  p:=ehead[now];
  sum:=0;
  while p<>-1 do
    begin
      i:=edge[p].adj;
      w:=edge[p].cost;
      if (w>0) and (dep[now]+1=dep[i]) and (sum<low) then
        begin
          tmp:=dfs(i,min(low-sum,w));
          if tmp>0 then
            begin
              edge[p].cost:=edge[p].cost-tmp;
              edge[p xor 1].cost:=edge[p xor 1].cost+tmp;
              sum:=sum+tmp;
            end;
        end;
      p:=edge[p].next;
    end;
  if sum=0 then dep[now]:=-1;
  exit(sum);
end;

function flow:double;
var
  tmp,ans:double;

begin
  ans:=0;
  while bfs(1,n) do
    repeat
      tmp:=dfs(1,maxlongint);
      if tmp<eps then break;
      ans:=ans+tmp;
    until false;
  exit(ans);
end;

begin
  while not eof do
    begin
      readln(n,m);
      l:=maxlongint;
      r:=0;
      for i:=1 to m do
        with data[i] do
          begin
            readln(src,dest,cost);
            l:=min(l,cost);
            r:=max(r,cost);
          end;
      while abs(l-r)<eps do
        begin
          mid:=(l+r)/2;
          res:=init(mid)+flow;
          if res>0 then l:=mid else r:=mid;
        end;
      init(r);
      flow;
      bfs(1,n);
      ans:=0;
      for i:=1 to m do
        with data[i] do
          if (cost<=r) or ((dep[src]<0) xor (dep[dest]<0)) then
            begin
              inc(ans);
              a[ans]:=i;
            end;
      writeln(ans);
      for i:=1 to ans do write(a[i],' ');
      writeln;
      writeln;
    end;
end.
