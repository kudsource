#include <cstdio>
#include <cmath>

using namespace std;

const int maxn=200000;

int n,tot=0;

int belong[maxn],k[maxn],jump[maxn],f[maxn],st[1000];

void init()
{
	scanf("%d",&n);
	int len=sqrt(n)+0.5,j=0;
	for (int i=0; i<n; ++i)
	{
		if (++j>len) j=0,st[++tot]=i;
		belong[i]=tot;
		scanf("%d",&k[i]);
	}
	belong[n]=tot+1;
	for (int i=n-1; i>=0; --i)
	{
		if (i+k[i]>=st[belong[i]+1]) f[i]=1,jump[i]=i+k[i]; else
			f[i]=f[i+k[i]]+1,jump[i]=jump[i+k[i]];
	}
}

int query(int x)
{
	int res=0;
	while (x<n)
	{
		res+=f[x];
		x=jump[x];
	}
	return res;
}

void modify(int x, int num)
{
	k[x]=num;
	for (int i=x; i>=st[belong[x]]; --i)
	{
		if (i+k[i]>=st[belong[i]+1]) f[i]=1,jump[i]=i+k[i]; else
			f[i]=f[i+k[i]]+1,jump[i]=jump[i+k[i]];
	}
}

int main()
{
	int m,opt,x,tmp;
	init();
	scanf("%d",&m);
	while (m--)
	{
		scanf("%d%d",&opt,&x);
		switch (opt)
		{
			case 1:
				printf("%d\n",query(x));
				break;
			case 2:
				scanf("%d",&tmp);
				modify(x,tmp);
				break;
		}
	}
	return 0;
}
