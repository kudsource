/**************************************************************
    Problem: 1499
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:864 ms
    Memory:1228 kb
****************************************************************/
 
#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>
     
const int maxn = 200+10;
 
const int way[][2] = {{-1,0},{1,0},{0,-1},{0,1}};
 
int n,m,x,y,k;
int dp[2][maxn][maxn];
int last = 0, now = 1;
 
char map[maxn][maxn];
 
std::deque< std::pair<int,int> > q;
 
void solve1(const int s, const int t) { //north
    for (int j=1; j<=m; ++j) {
        q.clear();
        q.push_back(std::pair<int,int>(n,dp[last][n][j] + n));
        dp[now][n][j] = dp[last][n][j];
        for (int i=n-1; i>=1; --i) {
            if (map[i][j] == 'x') {
                q.clear();
                continue;
            }
            while (!q.empty() && q.front().first - (t-s+1) > i) 
q.pop_front();
            dp[now][i][j] = dp[last][i][j];
            if (!q.empty()) dp[now][i][j] = 
std::max(dp[now][i][j],q.front().second - i);
            while (!q.empty() && q.back().second < dp[last][i][j] + i) 
q.pop_back();
            q.push_back(std::pair<int,int>(i,dp[last][i][j] + i));
        }
    }
}
 
 
void solve2(const int s, const int t) { //south
    for (int j=1; j<=m; ++j) {
        q.clear();
        q.push_back(std::pair<int,int>(1,dp[last][1][j] - 1));
        dp[now][1][j] = dp[last][1][j];
        for (int i=2; i<=n; ++i) {
            if (map[i][j] == 'x') {
                q.clear();
                continue;
            }
            while (!q.empty() && i - q.front().first > t - s + 1) 
q.pop_front();
            dp[now][i][j] = dp[last][i][j];
            if (!q.empty()) dp[now][i][j] = 
std::max(dp[now][i][j],q.front().second + i);
            while (!q.empty() && q.back().second < dp[last][i][j] - i) 
q.pop_back();
            q.push_back(std::pair<int,int>(i,dp[last][i][j] - i));
        }
    }
}
 
 
void solve3(const int s, const int t) { //west
    for (int i=1; i<=n; ++i) {
        q.clear();
        q.push_back(std::pair<int,int>(m,dp[last][i][m] + m));
        dp[now][i][m] = dp[last][i][m];
        for (int j=m-1; j>=1; --j) {
            if (map[i][j] == 'x') {
                q.clear();
                continue;
            }
            while (!q.empty() && q.front().first - (t-s+1) > j) 
q.pop_front();
            dp[now][i][j] = dp[last][i][j];
            if (!q.empty()) dp[now][i][j] = 
std::max(dp[now][i][j],q.front().second - j);
            while (!q.empty() && q.back().second < dp[last][i][j] + j) 
q.pop_back();
            q.push_back(std::pair<int,int>(j,dp[last][i][j] + j));
        }
    }
}
 
void solve4(const int s, const int t) { //east
    for (int i=1; i<=n; ++i) {
        q.clear();
        q.push_back(std::pair<int,int>(1,dp[last][i][1] - 1));
        dp[now][i][1] = dp[last][i][1];
        for (int j=2; j<=m; ++j) {
            if (map[i][j] == 'x') {
                q.clear();
                continue;
            }
            while (!q.empty() && j - q.front().first > t-s+1) 
q.pop_front();
            dp[now][i][j] = dp[last][i][j];
            if (!q.empty()) dp[now][i][j] = 
std::max(dp[now][i][j],q.front().second + j);
            while (!q.empty() && q.back().second < dp[last][i][j] - j) 
q.pop_back();
            q.push_back(std::pair<int,int>(j,dp[last][i][j] - j));
        }
    }
}
 
int main() {
    std::scanf("%d%d%d%d%d",&n,&m,&x,&y,&k);
    for (int i=1; i<=n; ++i)
        for (int j=1; j<=m; ++j)
            std::scanf(" %c",&map[i][j]);
    std::memset(dp,128,sizeof(dp));
    dp[last][x][y] = 0;
    while (k--) {
        static int s,t,d;
        std::scanf("%d%d%d",&s,&t,&d);
        dp[now][x][y] = 0;
        switch (d) {
            case 1:
                solve1(s,t);
                break;
            case 2:
                solve2(s,t);
                break;
            case 3:
                solve3(s,t);
                break;
            case 4:
                solve4(s,t);
                break;
        }
        last ^= 1;
        now ^= 1;
    }
    int ans = -1e9;
    for (int i=1; i<=n; ++i)
        for (int j=1; j<=m; ++j)
            ans = std::max(ans,dp[last][i][j]);
    std::printf("%d\n",ans);
    return 0;
}
