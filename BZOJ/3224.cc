/**************************************************************
    Problem: 3224
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:360 ms
    Memory:1752 kb
****************************************************************/
 
#include <cstdio>
#include <climits>
 
using namespace std;
 
struct node
{
    int key,size;
    node *son[2];
    node (int k, int s, node *ch): key(k), size(s) { son[0]=son[1]=ch; }
    inline void update() { size = son[0]->size + son[1]->size +1; }
} *nil=new node(0,0,NULL);
 
class SizeBalancedTree
{
    node *root;
    void rotate(node *&root, bool d) //0:Right Rotate, 1:Left Rotate
    {
        node *p=root->son[d];
        root->son[d]=p->son[d^1];
        p->son[d^1]=root;
        root->update();
        p->update();
        root=p;
    }
    void maintain(node *&root, bool d)
    {
        if (root==nil) return;
        node *&p=root->son[d];
        if (p->son[d]->size > root->son[d^1]->size) rotate(root,d); else
            if (p->son[d^1]->size > root->son[d^1]->size)
            {
                rotate(p,d^1);
                rotate(root,d);
            }
        else return;
        maintain(root->son[0],0);
        maintain(root->son[1],1);
        maintain(root,0);
        maintain(root,1);
    }
    void insert(node *&root, int x)
    {
        if (root==nil) { root=new node(x,1,nil); return; }
        bool d = (x > root->key);
        insert(root->son[d],x);
        maintain(root,d);
        root->update();
    }
    void remove(node *&root, int x)
    {
        if (root==nil) return;
        bool d;
        if (root->key == x)
        {
            if (root->son[1]==nil) { delete root; root=root->son[0]; return; }
            if (root->son[0]==nil) { delete root; root=root->son[1]; return; }
            node *p=root->son[1];
            while (p->son[0]!=nil) p=p->son[0];
            root->key=p->key;
            remove(root->son[1],p->key);
            d=true;
        }
        else
        {
            d = (x > root->key);
            remove(root->son[d],x);
        }
        maintain(root,d^1);
        root->update();
    }
    int select(node *root, int k)
    {
        int tmp=root->son[0]->size;
        if (tmp==k) return root->key;
        if (tmp>k) return select(root->son[0],k);
        return select(root->son[1],k-tmp-1);
    }
    int rank(node *root, int x)
    {
        if (root==nil) return 1;
        if (x <= root->key) return rank(root->son[0],x); else
            return root->son[0]->size+1+rank(root->son[1],x);
    }
    int pred(node *root, int x)
    {
        if (root==nil) return x;
        bool d = (x > root->key);
        int res=pred(root->son[d],x);
        if (d && res==x) res=root->key;
        return res;
    }
    int succ(node *root, int x)
    {
        if (root==nil) return x;
        bool d = (x >= root->key);
        int res=succ(root->son[d],x);
        if (!d && res==x) res=root->key;
        return res;
    }
 
public:
    SizeBalancedTree() { nil->son[0]=nil->son[1]=nil; root=nil; }
    inline void Insert(int x) { insert(root,x); }
    inline void Delete(int x) { remove(root,x); }
    inline int Select(int k)
    {
        if (k > root->size) return INT_MAX;
        return select(root,k-1);
    }
    inline int Rank(int x) { return rank(root,x); }
    inline int Pred(int x) { return pred(root,x); }
    inline int Succ(int x) { return succ(root,x); }
} *T = new SizeBalancedTree;
 
int main()
{
    int n;
    scanf("%d",&n);
    int opt,x,cnt=0;
    while (n--)
    {
        scanf("%d%d",&opt,&x);
        switch (opt)
        {
            case 1: T->Insert(x); break;
            case 2: T->Delete(x); break;
            case 3: printf("%d\n",T->Rank(x)); break;
            case 4: printf("%d\n",T->Select(x)); break;
            case 5: printf("%d\n",T->Pred(x)); break;
            case 6: printf("%d\n",T->Succ(x)); break;
        }
    }
    return 0;
}
