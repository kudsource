/**************************************************************
    Problem: 2034
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:604 ms
    Memory:196800 kb
****************************************************************/
 
#include <iostream>
#include <algorithm>
 
const int maxn = 5000 + 10;
 
struct node {
    int l,r,v;
} data[maxn];
 
inline bool cmp(const node &p, const node &q) { return p.v > q.v; }
 
short task[(size_t)(1e8 + 10)];
 
bool find(int x, int pos) {
    if (pos > data[x].r) return false;
    if (task[pos] == 0) {
        task[pos] = x;
        return true;
    }
    if (data[x].r > data[task[pos]].r) return find(x,pos + 1);
    if (find(task[pos],pos + 1)) {
        task[pos] = x;
        return true;
    }
    return false;
}
 
int main() {
    int n;
    std::cin>>n;
    for (int i = 1; i <= n; ++i)
        std::cin>>data[i].l>>data[i].r>>data[i].v;
    std::sort(data + 1, data + 1 + n, cmp);
    long long ans = 0;
    for (int i = 1; i <= n; ++i)
        if (find(i,data[i].l)) ans += data[i].v;
    std::cout<<ans<<std::endl;
    return 0;
}
