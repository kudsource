/**************************************************************
    Problem: 2876
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:452 ms
    Memory:1196 kb
****************************************************************/
 
#include <cstdio>
#include <cmath>
#include <algorithm>
 
using namespace std;
 
const int maxn = 1e4 + 10;
const double eps = 1e-12;
 
int n;
double e, v[maxn], k[maxn], s[maxn], rlim[maxn], x[maxn];
 
inline double sqr(double x) { return x * x; }
 
inline double cal_equ(double a, double b, double c, double d,
                           double x) {
  return a * x * x * x + b * x * x + c * x + d;
}
 
inline double cal_deriv(double a, double b, double c, double d,
                        double x) {
  return 3.0 * a * x * x + 2.0 * b * x + c;
}
 
double calc(double lambda, bool sw = false) {
  double res = 0;
  for (int i = 1; i <= n; ++i) {
    double x = max(v[i] * 1.5, 0.0) + 1, det;
    double a = 2.0 * lambda * k[i], b = -v[i] * a, c = 0, d = -1;
    while (fabs(det = cal_equ(a, b, c, d, x)) > eps) x -= det / cal_deriv(a, b, c, d, x);
    res += sw ? s[i] / x : k[i] * s[i] * sqr(x - v[i]);
  }
  return res;
}
 
int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  scanf("%d%lf", &n, &e);
  for (int i = 1; i <= n; ++i) scanf("%lf%lf%lf", s + i, k + i, v + i);
  double l = 0, r = 1;
  while (calc(r) > e) l = r, r *= 2;
  while (fabs(r - l) > eps) {
    double mid = (l + r) / 2;
    if (calc(mid) > e) l = mid; else r = mid;
  }
  printf("%.10lf\n", calc((l + r) / 2, true));
  return 0;
}