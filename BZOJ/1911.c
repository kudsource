/**************************************************************
    Problem: 1911
    User: dennisyang
    Language: C
    Result: Accepted
    Time:1708 ms
    Memory:20288 kb
****************************************************************/
 
/*
dp[i] = max{dp[j-1] + a*sqr(sum[i]-sum[j-1])+b*(sum[i]-sum[j-1])+c}
 
dp[k-1]+a*sqr(sum[i]-sum[k-1])+b*(sum[i]-sum[k-1]) > dp[j-1]+a*sqr(sum[i]-sum[j-1])+b*(sum[i]-sum[j-1])
dp[k-1] + a*(-2*sum[i]*sum[k-1]+sqr(sum[k-1])) + b*(-sum[k-1]) > dp[j-1] + a*(-2*sum[i]*sum[j-1]+sqr(sum[j-1])) + b*(-sum[j-1])
 
(dp[k-1]+a*sqr(sum[k-1])-b*sum[k-1]) - (dp[j-1]+a*sqr(sum[j-1])-b*sum[j-1]) > 2a*sum[i]*(sum[k-1]-sum[j-1])
 
y(k) = dp[k-1]+a*sqr(sum[k-1])-b*sum[k-1]
x(k) = 2a*sum[k-1]
 
y(k)-y(j) > sum[i]*(x(k)-x(j))
 
j<k (x(j) > x(k))
    slope < sum[i]
j>k (x(j) < x(k))
    slope > sum[i]
*/
#include <stdio.h>
 
#define sqr(x) ((x)*(x))
#define cal(x) (a*sqr(x)+b*(x)+c)
#define maxn 1000001
 
typedef long long int64;
 
int n,queue[maxn];
int64 a,b,c,sum[maxn],dp[maxn];
 
inline double y(int k) {
    return dp[k-1] + a*sqr(sum[k-1]) - b*sum[k-1];
}
 
inline double x(int k) {
    return 2*a*sum[k-1];
}
 
inline double slope(int p, int q) {
    return (y(p)-y(q)) / (x(p)-x(q));
}
 
int main() {
    scanf("%d",&n);
    scanf("%lld%lld%lld",&a,&b,&c);
    int i;
    for (i=1; i<=n; ++i) scanf("%lld",sum+i);
    for (i=1; i<=n; ++i) sum[i] += sum[i-1];
    int *qf = &queue[1], *qr = &queue[0];
    for (i=1; i<=n; ++i) {
        while (qf < qr && slope(*(qr-1),*qr) > slope(*qr,i)) --qr;
        *++qr = i;
        while (qf < qr && slope(*qf,*(qf+1)) < sum[i]) ++qf;
        dp[i] = dp[*qf - 1] + cal(sum[i] - sum[*qf - 1]);
    }
    printf("%lld\n",dp[n]);
    return 0;
}