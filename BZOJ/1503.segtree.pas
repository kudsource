const
  maxn=100010;

type
  segtree=^node;
  node=record
         l,r,sum:longint;
         lson,rson:segtree;
       end;

var
  root:segtree;
  n,m,delta,k,ans:longint;
  order:char;

procedure build(l,r:longint; var root:segtree);
var
  m:longint;

begin
  new(root);
  root^.l:=l;
  root^.r:=r;
  root^.sum:=0;
  if l=r then
    begin
      root^.lson:=nil;
      root^.rson:=nil;
      exit;
    end;
  m:=(l+r) div 2;
  build(l,m,root^.lson);
  build(m+1,r,root^.rson);
end;

procedure update(var root:segtree);
begin
  root^.sum:=root^.lson^.sum+root^.rson^.sum;
end;

procedure ins(k:longint; var root:segtree);
var
  m:longint;

begin
  if (root^.l=k) and (root^.r=k) then
    begin
      inc(root^.sum);
      exit;
    end;
  m:=(root^.l+root^.r) shr 1;
  if k<=m then ins(k,root^.lson) else ins(k,root^.rson);
  update(root);
end;

function del(l,r:longint; var root:segtree):longint;
var
  m,tmp:longint;

begin
  if root^.sum=0 then exit(0);
  if (root^.l=root^.r) then
    begin
      tmp:=root^.sum;
      root^.sum:=0;
      exit(tmp);
    end;
  m:=(root^.l+root^.r) shr 1;
  if r<=m then tmp:=del(l,r,root^.lson) else
    if l>m then tmp:=del(l,r,root^.rson) else
      tmp:=del(l,m,root^.lson)+del(m+1,r,root^.rson);
  update(root);
  exit(tmp);
end;

function find(k:longint; root:segtree):longint;
begin
  if root^.l=root^.r then exit(root^.l);
  if root^.lson^.sum>=k then exit(find(k,root^.lson)) else
    exit(find(k-root^.lson^.sum,root^.rson));
end;

begin
  readln(n,m);
  build(0,maxn*3,root);
  delta:=maxn;
  repeat
    dec(n);
    readln(order,k);
    case order of
      'I':if k>=m then ins(k+delta,root);
      'A':dec(delta,k);
      'S':begin
            inc(delta,k);
            inc(ans,del(0,m+delta-1,root));
          end;
      'F':if k>root^.sum then writeln(-1) else writeln(find(root^.sum+1-k,root)-delta);
    end;
  until n=0;
  writeln(ans);
end.
