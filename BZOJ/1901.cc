/**************************************************************
    Problem: 1901
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:1012 ms
    Memory:59508 kb
****************************************************************/
 
//Algorithm: Fenwick Tree & Persistent Segment Tree
#include <cstdio>
#include <vector>
 
const int maxnode = 5e6 + 10,maxn = 1e4 + 10,rlim = 1<<30;
 
struct node {
    int size;
    node *ch[2];
} pool[maxnode],*tot = &pool[0],*root[maxn];
 
int n,m,orig[maxn];
 
node* Insert(node* orig, int l, int r, int pos, int val) {
    if (orig == NULL) orig = tot++;
    orig->size += val;
    if (l == r) return orig;
    int m = (l + r) >> 1;
    if (pos <= m) orig->ch[0] = Insert(orig->ch[0],l,m,pos,val); else orig->ch[1] = Insert(orig->ch[1],m+1,r,pos,val);
    return orig;
}
 
inline int lowbit(int x) { return x & -x; }
 
inline void get(std::vector<node*> &tree, int idx) {
    tree.clear();
    while (idx) {
        tree.push_back(root[idx]);
        idx -= lowbit(idx);
    }
}
 
inline int getsum(std::vector<node*> &tree) {
    int res = 0;
    std::vector<node*>::iterator iter;
    for (iter = tree.begin(); iter != tree.end(); ++iter)
        if ((*iter) && (*iter) -> ch[0]) res += (*iter) -> ch[0] -> size;
    return res;
}
 
inline void turn(std::vector<node*> &tree, bool d) {
    std::vector<node*>::iterator iter;
    for (iter = tree.begin(); iter != tree.end(); ++iter)
        if (*iter) *iter = (*iter) -> ch[d];
}
 
inline void update(int idx, int pos, int val) {
    while (idx <= n) {
        root[idx] = Insert(root[idx],0,rlim,pos,val);
        idx += lowbit(idx);
    }
}
 
int Query(int l, int r, int k, std::vector<node*> lt, std::vector<node*> rt) {
    if (l == r) return l;
    int m = (l + r) >> 1, cnt = getsum(rt) - getsum(lt);
    bool d = k > cnt;
    turn(lt,d);
    turn(rt,d);
    if (d) {
        l = m + 1;
        k -= cnt;
    } else r = m;
    return Query(l,r,k,lt,rt);
}
 
inline int query(int l, int r, int k) {
    std::vector<node*> tree[2];
    get(tree[0],l-1);
    get(tree[1],r);
    return Query(0,rlim,k,tree[0],tree[1]);
}
 
int main() {
    std::scanf("%d%d",&n,&m);
    for (int i = 1,t; i <= n; ++i) {
        std::scanf("%d",orig + i);
        update(i,orig[i],1);
    }
    char op;
    int pos,num,l,r,k;
    while (m--) {
        std::scanf(" %c",&op);
        switch (op) {
            case 'C':
                std::scanf("%d%d",&pos,&num);
                update(pos,orig[pos],-1);
                update(pos,orig[pos] = num,1);
                break;
            case 'Q':
                std::scanf("%d%d%d",&l,&r,&k);
                std::printf("%d\n",query(l,r,k));
                break;
        }
    }
    return 0;
}
