/**************************************************************
    Problem: 1036
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:3740 ms
    Memory:4184 kb
****************************************************************/
 
#include <cstdio>
#include <cmath>
#include <limits>
#include <vector>
#include <algorithm>
 
#define foreach(i,x) for(__typeof(x.begin()) i=x.begin(); i!=x.end(); ++i)
 
using namespace std;
 
const int maxn = 30010;
 
int n;
vector<int> epool[maxn];
 
int father[maxn], dep[maxn], root[maxn];
int w[maxn], mx[maxn], sum[maxn];
int size[maxn], lim;
 
inline void update(int a) {
  if (a == root[a]) {
    sum[a] = mx[a] = w[a];
  } else {
    sum[a] = sum[father[a]] + w[a];
    mx[a] = max(mx[father[a]], w[a]);
  }
}
 
void dfs(int a, int top) {
  root[a] = top;
  ++size[top];
  update(a);
  foreach(iter, epool[a]) {
    int b = *iter;
    if (b != father[a]) {
      father[b] = a;
      dep[b] = dep[a] + 1;
      size[top] < lim ? dfs(b, top) : dfs(b, b);
    } 
  }
}
 
void dfs_modify(int a) {
  update(a);
  foreach(iter, epool[a]) {
    int b = *iter;
    if (b != father[a] && root[b] == root[a])
      dfs_modify(b);
  }
}
 
int qmax(int a, int b) {
  if (root[a] == root[b]) {
    int res = numeric_limits<int>::min();
    while (a != b) {
      if (dep[a] < dep[b]) swap(a, b);
      res = max(res, w[a]);
      a = father[a];
    }
    return max(res, w[a]);
  } else {
    if (dep[root[a]] < dep[root[b]]) swap(a, b);
    return max(mx[a] ,qmax(father[root[a]], b));
  }
}
 
int qsum(int a, int b) {
  if (root[a] == root[b]) {
    int res = 0;
    while (a != b) {
      if (dep[a] < dep[b]) swap(a, b);
      res += w[a];
      a = father[a];
    }
    return res + w[a];
  } else {
    if (dep[root[a]] < dep[root[b]]) swap(a, b);
    return sum[a] + qsum(father[root[a]], b);
  }
}
 
int main() {
  scanf("%d", &n);
  lim = static_cast<int>(sqrt(n)) + 1;
  int a, b;
  for (int i = 1; i < n; ++i) {
    scanf("%d%d", &a, &b);
    epool[a].push_back(b);
    epool[b].push_back(a);
  }
  for (int i = 1; i <= n; ++i) scanf("%d", w + i);
  dfs(1, 1);
  int tcase;
  char op[10];
  scanf("%d", &tcase);
  while (tcase--) {
    scanf(" %s%d%d", op, &a, &b);
    switch (op[1]) {
      case 'H': //CHANGE
        w[a] = b;
        dfs_modify(a);
        break;
      case 'M': //QMAX
        printf("%d\n", qmax(a, b));
        break;
      case 'S': //QSUM
        printf("%d\n", qsum(a, b));
        break;
    }
  }
  return 0;
}