/**************************************************************
    Problem: 1012
    User: dennisyang
    Language: C
    Result: Accepted
    Time:1188 ms
    Memory:19504 kb
****************************************************************/
 
#include <stdio.h>
#include <limits.h>
 
#define maxn 200010
 
typedef long long int64;
 
int n;
int64 mod;
 
typedef struct _node {
    int l,r;
    int64 val;
    struct _node *ch[2];
} node;
 
node pool[maxn * 4],*root,*tot = &pool[0];
 
inline int mid(const node *p) { return (p->l + p->r) >> 1; }
 
inline int64 max(int64 p, int64 q) { return p>q ? p:q; }
 
inline void update(node *p) { p->val = max(p->ch[0]->val,p->ch[1]->val); }
 
node* build(int l, int r) {
    node* cur = tot++;
    cur->l = l, cur->r = r;
    if (l < r) {
        cur->ch[0] = build(l,mid(cur));
        cur->ch[1] = build(mid(cur)+1,r);
    };
    return cur;
}
 
int64 query(node* cur, int l, int r) {
    if (l <= cur->l && cur->r <= r) return cur->val;
    int64 res = 0;
    if (l <= mid(cur)) res = max(res,query(cur->ch[0],l,r));
    if (r > mid(cur)) res = max(res,query(cur->ch[1],l,r));
    return res;
}
 
void modify(node* cur, int pos, int64 val) {
    if (cur->l == pos && pos == cur->r) {
        cur->val = val;
        return;
    }
    if (pos <= mid(cur)) modify(cur->ch[0],pos,val); else modify(cur->ch[1],pos,val);
    update(cur);
}
 
int main() {
    pool[0].val = INT_MAX;
    scanf("%d%lld",&n,&mod);
    root = build(1,n);
    char op;
    int len = 0;
    int64 k = 0,ans = 0;
    while (n--) {
        scanf(" %c%lld",&op,&k);
        switch (op) {
            case 'A':
                modify(root,++len,(ans + k) % mod);
                break;
            case 'Q':
                ans = query(root,len-k+1,len);
                printf("%lld\n",ans);
                break;
        }
    }
    return 0;
}