/**************************************************************
    Problem: 1207
    User: dennisyang
    Language: C
    Result: Accepted
    Time:2552 ms
    Memory:908 kb
****************************************************************/
 
#include <stdio.h>
#include <math.h>
 
#define maxm 10010
#define max(p,q) ((p)>(q)?(p):(q))
 
int n,m,time[maxm],x[maxm],y[maxm];
int dp[maxm];
 
int main() {
    scanf("%d%d",&n,&m);
    register int i,j;
    int ans = 0;
    for (i=1; i<=m; ++i) {
        scanf("%d%d%d",time+i,x+i,y+i);
        dp[i] = 1;
        for (j=1; j<i; ++j)
            if ((abs(x[i]-x[j])+abs(y[i]-y[j])) <= (time[i]-time[j]))
                dp[i] = max(dp[i],dp[j]+1);
        ans = max(ans,dp[i]);
    }
    printf("%d\n",ans);
    return 0;
}