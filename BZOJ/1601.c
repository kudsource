/**************************************************************
    Problem: 1601
    User: dennisyang
    Language: C
    Result: Accepted
    Time:68 ms
    Memory:1128 kb
****************************************************************/
 
#include <stdio.h>
#include <stdbool.h>
 
#define maxn 310
 
int n,cost[maxn][maxn],dis[maxn];
bool vis[maxn];
 
int main() {
    scanf("%d",&n);
    register int i,j;
    for (i=1; i<=n; ++i) {
        scanf("%d",&cost[0][i]);
        dis[i] = cost[i][0] = cost[0][i];
    }
    for (i=1; i<=n; ++i)
        for (j=1; j<=n; ++j)
            scanf("%d",&cost[i][j]);
    int ans = 0;
    vis[0] = true;
    for (i=0; i<n; ++i) {
        int k = -1;
        for (j=0; j<=n; ++j)
            if (!vis[j] && (dis[j] < dis[k] || k == -1)) k = j;
        vis[k] = true;
        ans += dis[k];
        for (j=0; j<=n; ++j)
            if (!vis[j] && cost[k][j] < dis[j]) dis[j] = cost[k][j];
    }
    printf("%d\n",ans);
    return 0;
}