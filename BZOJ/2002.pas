const
  maxn=100010;

var
  tree:array [0..maxn] of record
                            size,parent,d:longint;
                            //size: total
                            //parent: ...
                            //d: relationship with parent(0:lson, 1:rson)
                            son:array [0..1] of longint;
                            isroot:boolean;
                          end;
  m,n,i,x,y,z:longint;

procedure set_son(p,c,d:longint); //let c become p's d-th son
begin
  tree[p].son[d]:=c;
  tree[c].parent:=p;
  tree[c].d:=d;
end;

procedure update(k:longint);
begin
  with tree[k] do size:=tree[son[0]].size+tree[son[1]].size+1;
end;

procedure rotate(root:longint);
var
  p,d:longint;

begin
  p:=tree[root].parent;
  d:=tree[root].d;
  if tree[p].isroot then
    begin
      tree[p].isroot:=false;
      tree[root].isroot:=true;
      tree[root].parent:=tree[p].parent;
    end
  else set_son(tree[p].parent,root,tree[p].d);
  set_son(p,tree[root].son[1-d],d);
  set_son(root,p,1-d);
  update(p);
  update(root);
end;

procedure splay(root:longint);
begin
  while not tree[root].isroot do
    begin
      if tree[tree[root].parent].isroot then rotate(root) else
        if tree[root].d=tree[tree[root].parent].d then
          begin
            rotate(tree[root].parent);
            rotate(root);
          end
        else
          begin
            rotate(root);
            rotate(root);
          end;
    end;
end;

procedure access(x:longint);
var
  tmp:longint;

begin
  tmp:=0;
  repeat
    splay(x);
    tree[tree[x].son[1]].isroot:=true;
    tree[tmp].isroot:=false;
    set_son(x,tmp,1);
    update(x);
    tmp:=x;
    x:=tree[x].parent;
  until x=0;
end;

procedure link(p,q:longint);
begin
  access(p);
  tree[p].parent:=q;
  access(p);
end;

procedure cut(p:longint);
begin
  access(p);
  splay(p);
  tree[tree[p].son[0]].isroot:=true;
  tree[tree[p].son[0]].parent:=0;
  set_son(p,0,0);
  update(p);
end;

begin
  readln(n);
  for i:=1 to n do
    begin
      read(x);
      tree[i].size:=1;
      tree[i].isroot:=true;
      if x+i>n then tree[i].parent:=n+1 else tree[i].parent:=x+i;
    end;
  tree[n+1].size:=1;
  tree[n+1].isroot:=true;
  readln(m);
  for i:=1 to m do
    begin
      read(x);
      if x=1 then
        begin
          readln(y);
          inc(y);
          access(y);
          splay(y);
          writeln(tree[tree[y].son[0]].size);
        end
      else
        begin
          readln(y,z);
          inc(y);
          cut(y);
          if y+z>n then link(y,n+1) else link(y,y+z);
        end;
    end;
end.