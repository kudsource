uses math;

const
  maxm=1000000;
  maxn=100000;

var
  e:array [0..maxm] of record
                         adj,next,cap:longint;
                       end;
  h,dis:array [0..maxn] of longint;
  sum,n,m,s,t,cnt:longint;

procedure link(p,q,c:longint);
begin
  e[cnt].adj:=q;
  e[cnt].next:=h[p];
  e[cnt].cap:=c;
  h[p]:=cnt;
  inc(cnt);
  e[cnt].adj:=p;
  e[cnt].next:=h[q];
  e[cnt].cap:=0;
  h[q]:=cnt;
  inc(cnt);
end;

procedure init;
var
  i,p,a,b,c:longint;

begin
  readln(n,m);
  fillchar(h,sizeof(h),255);
  s:=0;
  t:=n+m+1;
  cnt:=0;
  for i:=1 to n do
    begin
      read(p);
      link(i,t,p);
    end;
  sum:=0;
  for i:=1 to m do
    begin
      readln(a,b,c);
      link(n+i,a,maxlongint);
      link(n+i,b,maxlongint);
      link(s,n+i,c);
      inc(sum,c);
    end;
end;

function bfs(s,t:longint):boolean;
var
  q:array [0..maxn] of longint;
  qf,qr:^longint;
  p:longint;

begin
  qf:=@q[0];
  qr:=@q[1];
  qr^:=s;
  fillchar(dis,sizeof(dis),255);
  dis[s]:=0;
  repeat
    inc(qf);
    p:=h[qf^];
    while p<>-1 do
      begin
        if (dis[e[p].adj]=-1) and (e[p].cap>0) then
          begin
            inc(qr);
            qr^:=e[p].adj;
            dis[qr^]:=dis[qf^]+1;
            if qr^=t then exit(true);
          end;
        p:=e[p].next;
      end;
  until qf>=qr;
  exit(false);
end;

function dfs(now,low:longint):longint;
var
  p,sum,tmp:longint;

begin
  if now=t then exit(low);
  p:=h[now];
  sum:=0;
  while p<>-1 do
    begin
      if (dis[e[p].adj]=dis[now]+1) and (e[p].cap>0) and (sum<low) then
        begin
          tmp:=dfs(e[p].adj,min(low-sum,e[p].cap));
          if tmp>0 then
            begin
              inc(sum,tmp);
              dec(e[p].cap,tmp);
              inc(e[p xor 1].cap,tmp);
            end;
        end;
      p:=e[p].next;
    end;
  if sum=0 then dis[now]:=-1;
  exit(sum);
end;

function dinic(s,t:longint):longint;
var
  tmp:longint;

begin
  dinic:=0;
  while bfs(s,t) do
    repeat
      tmp:=dfs(s,maxlongint);
      inc(dinic,tmp);
    until tmp=0;
end;

begin
  init;
  writeln(sum-dinic(s,t));
end.