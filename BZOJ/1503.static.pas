const
  maxn=100010;

type
  node=record
         l,r,priority,sum,key:longint;
       end;

var
  treap:array [0..maxn] of node;
  top,n,m,k,root,ans:longint;
  order:char;

procedure lr(var root:longint); //left rotate
var
  tmp:longint;

begin
  tmp:=treap[root].r;
  treap[root].r:=treap[tmp].l;
  treap[tmp].l:=root;
  treap[tmp].sum:=treap[root].sum;
  treap[root].sum:=1+treap[treap[root].l].sum+treap[treap[root].r].sum;
  root:=tmp;
end;

procedure rr(var root:longint); //right rotate
var
  tmp:longint;

begin
  tmp:=treap[root].l;
  treap[root].l:=treap[tmp].r;
  treap[tmp].r:=root;
  treap[tmp].sum:=treap[root].sum;
  treap[root].sum:=1+treap[treap[root].l].sum+treap[treap[root].r].sum;
  root:=tmp;
end;

procedure ins(k:longint; var root:longint);
begin
  if root=0 then
    begin
      inc(top);
      root:=top;
      treap[top].priority:=random(maxlongint);
      treap[top].key:=k;
      treap[top].sum:=1;
      exit;
    end;
  inc(treap[root].sum);
  if k<treap[root].key then
    begin
      ins(k,treap[root].l);
      if treap[treap[root].l].priority<treap[root].priority then rr(root);
    end
  else
    begin
      ins(k,treap[root].r);
      if treap[treap[root].r].priority<treap[root].priority then lr(root);
    end;
end;

procedure add(k,root:longint);
begin
  if root=0 then exit;
  inc(treap[root].key,k);
  add(k,treap[root].l);
  add(k,treap[root].r);
end;

function del(min:longint; var root:longint):longint;
var
  tmp:longint;

begin
  if root=0 then exit(0);
  if treap[root].key<min then
    begin
      tmp:=treap[treap[root].l].sum+1;
      root:=treap[root].r;
      exit(del(min,root)+tmp);
    end
  else
    begin
      tmp:=del(min,treap[root].l);
      dec(treap[root].sum,tmp);
      exit(tmp);
    end;
end;

function find(k,root:longint):longint;
begin
  if treap[treap[root].l].sum+1=k then exit(treap[root].key);
  if treap[treap[root].l].sum+1>k then exit(find(k,treap[root].l)) else
    exit(find(k-treap[treap[root].l].sum-1,treap[root].r));
end;

begin
  readln(n,m);
  root:=0;
  repeat
    dec(n);
    readln(order,k);
    case order of
      'I':if k>=m then ins(k,root);
      'A':add(k,root);
      'S':begin
            add(-k,root);
            inc(ans,del(m,root));
          end;
      'F':if k>treap[root].sum then writeln(-1) else
            writeln(find(treap[root].sum-k+1,root));
    end;
  until n=0;
  writeln(ans);
end.
