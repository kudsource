#include <cstdio>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long int64;

const int mod = 51061, maxn = 1e5 + 10;

vector<int> epool[maxn];

struct node {
  node *p, *ch[2];
  bool rev;
  int add, mul;
  int sum, val, size;
  inline bool dir();
  inline bool isroot();
  inline void update();
  inline void relax();
  inline void clear_add(int64);
  inline void clear_mul(int64);
} pool[maxn], *nil = pool;

inline bool node::dir() {
  return this == p->ch[1];
}

inline bool node::isroot() {
  return !(this == p->ch[0] || this == p->ch[1]);
}

inline void node::update() {
  sum = (val + ch[0]->sum + ch[1]->sum) % mod;
  size = ch[0]->size + ch[1]->size + 1;
}

inline void node::relax() {
  if (rev) {
    rev = false;
    swap(ch[0], ch[1]);
    ch[0]->rev ^= 1, ch[1]->rev ^= 1;
  }
  if (mul != 1) {
    if (ch[0] != nil) ch[0]->clear_mul(mul);
    if (ch[1] != nil) ch[1]->clear_mul(mul);
    mul = 1;
  }
  if (add) {
    if (ch[0] != nil) ch[0]->clear_add(add);
    if (ch[1] != nil) ch[1]->clear_add(add);
    add = 0;
  }
}

inline void node::clear_add(int64 a) {
  sum = (sum + a * size) % mod;
  add += a;
  val += a;
}

inline void node::clear_mul(int64 m) {
  mul = (mul * m) % mod;
  add = (add * m) % mod;
  val = (val * m) % mod;
  sum = (sum * m) % mod;
}

inline void makenode(node *u) {
  u->p = u->ch[0] = u->ch[1] = nil;
  u->add = u->rev = 0;
  u->size = u->sum = u->val = u->mul = 1;
} 

inline void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  bool d = u->dir();
  u->p = v->p;
  if (!v->isroot()) v->p->ch[v->dir()] = u;
  if (u->ch[d^1] != nil) u->ch[d^1]->p = v;
  v->ch[d] = u->ch[d^1];
  u->ch[d^1] = v;
  v->p = u;
  v->update(), u->update();
}

inline void splay(node *u) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      u->dir() == u->p->dir() ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

inline node* expose(node *u) {
  node *v;
  for (v = nil; u != nil; u = u->p) {
    splay(u);
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void evert(node *u) {
  expose(u)->rev ^= 1;
  splay(u);
}

inline void link(node *u, node *v) {
  evert(u);
  u->p = v;
}

inline void cut(node *u, node *v) {
  evert(u);
  expose(v);
  splay(v);
  u->p = v->ch[0] = nil;
  v->update();
}

inline void addpath(node *u, node *v, int x) {
  evert(u);
  expose(v)->clear_add(x);
  splay(v);
}

inline void mulpath(node *u, node *v, int x) {
  evert(u);
  expose(v)->clear_mul(x);
  splay(v);
}

inline int query(node *u, node *v) {
  evert(u);
  return expose(v)->sum;
}

inline void bfs(int s) {
  vector<int>::iterator iter;
  queue<int> q;
  bool vis[maxn] = {0};
  q.push(s);
  vis[s] = true;
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    for (iter = epool[a].begin(); iter != epool[a].end(); ++iter) {
      if (!vis[*iter]) {
        pool[*iter].p = pool + a;
        vis[*iter] = true;
        q.push(*iter);
      }
    }
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n, tcase;
  scanf("%d%d", &n, &tcase);
  int u, v, p, q;
  nil->p = nil->ch[0] = nil->ch[1] = nil;
  for (int i = 1; i < n; ++i) {
    scanf("%d%d", &u, &v);
    epool[u].push_back(v);
    epool[v].push_back(u);
  }
  for (int i = 1; i <= n; ++i) makenode(pool + i);
  bfs(1);
  char op;
  while (tcase--) {
    scanf(" %c%d%d", &op, &u, &v);
    switch (op) {
      case '+':
        scanf("%d", &p);
        addpath(pool + u, pool + v, p);
        break;
      case '-':
        scanf("%d%d", &p, &q);
        cut(pool + u, pool + v);
        link(pool + p, pool + q);
        break;
      case '*':
        scanf("%d", &p);
        mulpath(pool + u, pool + v, p);
        break;
      case '/':
        printf("%d\n", query(pool + u, pool + v));
        break;
    }
  }
  return 0;
}
