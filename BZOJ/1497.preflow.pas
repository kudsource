uses math;
 
const
  maxm=1000000;
  maxn=100000;
  maxq=1 shl 20;
 
var
  e:array [0..maxm] of record
                         adj,next,cap:longint;
                       end;
  h,d,l:array [0..maxn] of longint;
  q:array [0..maxq] of longint;
  v:array [0..maxn] of boolean;
  sum,n,m,s,t,cnt,f,r,size:longint;
 
procedure link(p,q,c:longint);
begin
  e[cnt].adj:=q;
  e[cnt].next:=h[p];
  e[cnt].cap:=c;
  h[p]:=cnt;
  inc(cnt);
  e[cnt].adj:=p;
  e[cnt].next:=h[q];
  e[cnt].cap:=0;
  h[q]:=cnt;
  inc(cnt);
end;

procedure init;
var
  i,p,a,b,c:longint;
 
begin
  readln(n,m);
  fillchar(h,sizeof(h),255);
  fillchar(v,sizeof(v),false);
  s:=0;
  t:=n+m+1;
  cnt:=0;
  for i:=1 to n do
    begin
      read(p);
      link(i,t,p);
    end;
  sum:=0;
  for i:=1 to m do
    begin
      readln(a,b,c);
      link(n+i,a,maxlongint);
      link(n+i,b,maxlongint);
      link(s,n+i,c);
      inc(sum,c);
    end;
end;
 
procedure preflow(s:longint);
var
  p:longint;
 
begin
  p:=h[s];
  while p<>-1 do
    begin
      d[e[p].adj]:=e[p].cap;
      inc(e[p xor 1].cap,d[e[p].adj]);
      if (e[p].adj<>s) and (e[p].adj<>t) then
        begin
          r:=(r+1) mod maxn;
          q[r]:=e[p].adj;
          v[q[r]]:=true;
        end;
      p:=e[p].next;
    end;
end;
 
procedure push(s,t,p:longint);
var
  k:longint;
 
begin
  k:=min(e[p].cap,d[s]);
  dec(d[s],k);
  dec(e[p].cap,k);
  inc(d[t],k);
  inc(e[p xor 1].cap,k);
end;
 
procedure relabel(k:longint);
var
  p:longint;
 
begin
  p:=h[k];
  while p<>-1 do
    begin
      if e[p].cap>0 then l[k]:=min(l[k],l[e[p].adj]);
      p:=e[p].next;
    end;
  inc(l[k]);
end;
 
procedure discharge(k:longint);
var
  p:longint;
 
begin
  while d[k]>0 do
    begin
      p:=h[k];
      while p<>-1 do
        begin
          if (l[k]=l[e[p].adj]+1) and (e[p].cap>0) then
            begin
              push(k,e[p].adj,p);
              if (d[e[p].adj]>0) and (not v[e[p].adj]) and (e[p].adj<>s) and (e[p].adj<>t) then
                begin
                  r:=(r+1) mod maxq;
                  q[r]:=e[p].adj;
                  v[q[r]]:=true;
                end;
              if d[k]=0 then exit;
            end;
          p:=e[p].next;
        end;
      relabel(k);
    end;
end;
 
function flow(s,t:longint):longint;
var
  p:longint;
 
begin
  l[s]:=n+m; //(n+m+1)-1
  f:=0;
  r:=0;
  preflow(s);
  repeat
    f:=(f+1) mod maxq;
    v[q[f]]:=false;
    discharge(q[f]);
  until f>=r; //trick, in fact I should use f=r to check if the queue is empty
  exit(d[t]);
end;
 
begin
  init;
  writeln(sum-flow(s,t));
end.