#include <cstdio>
#include <cstring>

using namespace std;

const int maxn=1000000+10;

struct node
{
	int time;
	node *next[26],*fail;
} pool[250000],*root,*queue[250000];

int cnt=0;

char text[maxn];

namespace ac
{	
	void insert(char s[])
	{
		node *now=root;
		int n=strlen(s);
		for (int i=0; i<n; ++i)
		{
			int ch=s[i]-'a';
			if (!now->next[ch]) now->next[ch]=&pool[cnt++];
			now=now->next[ch];
		}
		now->time++;
	}
	
	void build()
	{
		int qf,qr;
		qf=qr=0;
		queue[++qr]=root;
		while (++qf<=qr)
		{
			for (int ch=0; ch<26; ++ch)
				if (queue[qf]->next[ch])
				{
					queue[++qr]=queue[qf]->next[ch];
					if (queue[qf]==root)
					{
						queue[qf]->next[ch]->fail=root;
						continue;
					}
					node *tmp=queue[qf]->fail;
					while (tmp!=root && !tmp->next[ch]) tmp=tmp->fail;
					if (tmp->next[ch]) tmp=tmp->next[ch];
					queue[qf]->next[ch]->fail=tmp;
				}
		}
	}
	
	int match(char s[])
	{
		int n=strlen(s),res=0;
		node *now=root;
		for (int i=0; i<n; ++i)
		{
			int ch=s[i]-'a';
			while (now!=root && !now->next[ch]) now=now->fail;
			if (now->next[ch]) now=now->next[ch];
			node *tmp=now;
			while (tmp!=root)
			{
				res+=tmp->time;
				tmp->time=0;
				tmp=tmp->fail;
			}
		}
		return res;
	}
}

int main()
{
	int tcase,n;
	scanf("%d",&tcase);
	while (tcase--)
	{
		memset(pool,0,sizeof(pool));
		cnt=0;
		root=&pool[cnt++];
		scanf("%d",&n);
		char pattern[50];
		while (n--)
		{
			scanf("%s",pattern);
			ac::insert(pattern);
		}
		ac::build();
		scanf("%s",text);
		printf("%d\n",ac::match(text));
	}
}
