#include <cstdio>
#include <cstring>
#include <algorithm>

#define lch (i<<1)
#define rch ((i<<1)|1)

using namespace std;

const int maxn = 1 << 20;

int n, m, tree[maxn], det;

inline int query(int l, int r) {
  int res = 0;
  for (l = l + det - 1, r = r + det + 1; l^r^1; l >>= 1, r >>= 1) {
    if (~l&1) res = max(res, tree[l^1]);
    if (r&1) res = max(res, tree[r^1]);
  }
  return res;
}

inline void modify(int i, int val) {
  tree[i += det] = val;
  while (i) {
    i >>= 1;
    tree[i] = max(tree[lch], tree[rch]);
  }
}

int main() {
  while (scanf("%d%d", &n, &m) != EOF) {
    memset(tree, 0, sizeof tree);
    for (det = 1; det <= n; det <<= 1) {}
    for (int i = 1; i <= n; ++i) scanf("%d", tree + det + i);
    for (int i = det - 1; i > 0; --i) tree[i] = max(tree[lch], tree[rch]);
    char op;
    int a, b;
    while (m--) {
      scanf(" %c%d%d", &op, &a, &b);
      switch (op) {
        case 'Q':
          printf("%d\n", query(a, b));
          break;
        case 'U':
          modify(a, b);
          break;
      }
    }
  }
  return 0;
}
