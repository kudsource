#include <stdio.h>
#include <string.h>
#include <math.h>

#define maxn 200010
#define max(p,q) ((p)>(q)?(p):(q))

int n,k,table[maxn][20],res[maxn];

void init() {
    int m = res[n],i,j;
    for (j=1; j<=m; ++j)
        for (i=1; i+(1<<j)-1<=n; ++i)
            table[i][j] = max(table[i][j-1],table[i+(1<<(j-1))][j-1]);
}

int query(int l, int r) {
    int j = res[r-l+1];
    return max(table[l][j],table[r-(1<<j)+1][j]);
}

int main() {
    int m,step,cnt,i,j;
    for (i=1; i<maxn; ++i) res[i] = (int)(log((double)i)/log(2.0));
    while (scanf("%d%d",&n,&k)) {
        if (n < 0 && k < 0) break;
        memset(table,0,sizeof table);
        for (i=1; i<=n; ++i) scanf("%d",&table[i][0]);
        init();
        for (step=n; step>0; --step) {
            cnt = j = 0;
            for (i=1; i+step-1<=n; i+=step) {
                cnt += query(i,i+step-1);
                ++j;
                if (cnt > k && n/j == step) {
                    printf("%d\n",j);
                    break;
                }
            }
            if (i+step-1 <= n) break;
        }
        if (step == 0) puts("-1");
    }
    return 0;
}
