#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define maxn 16010
#define red 1
#define black 2

typedef struct _edge {
    int adj;
    struct _edge *next;
} edge;

edge epool[maxn*10],*ehead[maxn],*etot;

int n,m,cnt,ans[maxn],col[maxn];

void link(int p, int q) {
    etot->adj = q;
    etot->next = ehead[p];
    ehead[p] = etot++;
}

int dfs(int i) {
    if (col[i]) return col[i];
    col[i] = red;
    col[i^1] = black;
    ans[cnt++] = i;
    edge *p;
    for (p = ehead[i]; p != NULL; p = p->next) {
        int j = p->adj;
        if (dfs(j) == black) return black;
    }
    return red;
}

int main() {
    while (scanf("%d%d",&n,&m) != EOF) {
        n <<= 1;
        memset(ehead,0,sizeof ehead);
        etot = &epool[0];
        while (m--) {
            int p,q;
            scanf("%d%d",&p,&q);
            --p, --q;
            link(p,q^1);
            link(q,p^1);
        }
        memset(col,0,sizeof col);
        cnt = 0;
        int i,j;
        bool flag = true;
        for (i = 0; i < n && flag; ++i) {
            if (col[i]) continue;
            cnt = 0;
            if (dfs(i) == black) {
                for (j = 0; j < cnt; ++j) col[ans[j]] = col[ans[j]^1] = 0;
                if (dfs(i^1) == black) {
                    flag = false;
                    break;
                }
            }
        }
        if (!flag) {
            puts("NIE");
            continue;
        }
        for (i = 0; i < n; ++i) if (col[i] == red) printf("%d\n",i+1);
    }
    return 0;
}
