#include <stdio.h>
#include <string.h>

#define maxn 40010
#define max(p,q) ((p)>(q)?(p):(q))

int n;

typedef struct _edge {
    int adj,w;
    struct _edge *next;
} edge;

edge epool[maxn*2],*ehead[maxn],*tot;

void link(int p, int q, int w) {
    tot->adj = q;
    tot->next = ehead[p];
    tot->w = w;
    ehead[p] = tot++;
}

int father[maxn],top[maxn],size[maxn],son[maxn],cost[maxn],map[maxn],dep[maxn],cnt;

void dfs(int i) {
    size[i] = 1;
    son[i] = 0;
    int j;
    edge *p;
    for (p = ehead[i]; p != NULL; p = p->next) {
        j = p->adj;
        if (j != father[i]) {
            father[j] = i;
            cost[j] = p->w;
            dep[j] = dep[i] + 1;
            dfs(j);
            size[i] += size[j];
            if (size[j] > size[son[i]]) son[i] = j;
        }
    }
}

void split(int i, int tp) {
    map[i] = ++cnt;
    top[i] = tp;
    if (son[i]) split(son[i],tp);
    edge *p;
    int j;
    for (p = ehead[i]; p != NULL; p = p->next) {
        j = p->adj;
        if (j != father[i] && j != son[i])
            split(j,j);
    }
}

int tree[maxn];

int lowbit(int x) { return x & -x; }

void update(int pos, int x) {
    while (pos <= n) {
        tree[pos] += x;
        pos += lowbit(pos);
    }
}

int q(int pos) {
    int res = 0;
    while (pos) {
        res += tree[pos];
        pos -= lowbit(pos);
    }
    return res;
}

int query(int l, int r) { return q(r) - q(l-1); }

void swap(int *p, int *q) {
    int t = *p;
    *p = *q;
    *q = t;
}

int lca_qsum(int p, int q) {
    int res = 0;
    int t1 = top[p], t2 = top[q];
    while (t1 != t2) {
        if (dep[t1] < dep[t2]) swap(&t1,&t2), swap(&p,&q);
        res += query(map[t1],map[p]);
        p = father[t1], t1 = top[p];
    }
    if (p == q) return res;
    if (dep[p] > dep[q]) swap(&p,&q);
    return res + query(map[son[p]],map[q]);
}

int main() {
    int tcase,m;
    scanf("%d",&tcase);
    while (tcase--) {
        scanf("%d%d",&n,&m);
        int i,s,t,w;
        memset(ehead,NULL,sizeof ehead);
        tot = &epool[0];
        cnt = 0;
        for (i = 1; i < n; ++i) {
            scanf("%d%d%d",&s,&t,&w);
            link(s,t,w);
            link(t,s,w);
        }
        dep[1] = 0;
        dfs(1);
        split(1,1);
        memset(tree,0,sizeof tree);
        for (i = 1; i <= n; ++i) update(map[i],cost[i]);
        int p,q;
        while (m--) {
            scanf("%d%d",&p,&q);
            printf("%d\n",lca_qsum(p,q));
        }
    }
    return 0;
}
