#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#define maxn 10010
#define maxm 20010
#define lg(x) (log(x)/log(2.0))
#define min(p,q) ((p)<(q)?(p):(q))

int n,m,tcase;
const int vroot = 0;

typedef struct _edge {
	int adj,w;
	struct _edge *next;
} edge;

edge epool[maxm],*ehead[maxn],*etot = &epool[0];

void link(int p, int q, int w) {
	etot->adj = q;
	etot->next = ehead[p];
	etot->w = w;
	ehead[p] = etot++;
}

int f[maxn];

int find(int x) {
	if (f[x] == x) return x; else
		return f[x] = find(f[x]);
}

void merge(int p, int q) { f[find(p)] = find(q); }

bool vis[maxn];
int dep[maxn],dis[maxn],first[maxn];
int idx[maxn*2],depidx[maxn*2],cnt = 0;

void dfs(int i) {
	vis[i] = true;
	idx[++cnt] = i;
	depidx[cnt] = dep[i];
	if (!first[i]) first[i] = cnt;
	edge *p;
	for (p = ehead[i]; p != NULL; p = p->next) {
		int j = p->adj;
		if (!vis[j]) {
			dis[j] = dis[i] + p->w;
			dep[j] = dep[i] + 1;
			dfs(j);
			idx[++cnt] = i;
			depidx[cnt] = dep[i];
		}
	}
}

int table[maxn*2][20];

void rmq_init() {
	int i,j,k;
	for (i=1; i<=cnt; ++i) table[i][0] = i;
	for (j=1; (1<<j)<=cnt; ++j) {
		k = 1<<(j-1);
		for (i=1; i+k<cnt; ++i)
			table[i][j] = (depidx[table[i][j-1]] <= depidx[table[i+k][j-1]]) ? table[i][j-1] : table[i+k][j-1]; 
	}
}

int rmq(int l, int r) {
	int k = lg(r-l+1);
	return (depidx[table[l][k]] <= depidx[table[r-(1<<k)+1][k]]) ? table[l][k] : table[r-(1<<k)+1][k];
}

void swap(int *p, int *q) {
	int t = *p;
	*p = *q;
	*q = t;
}

int main() {
	while (scanf("%d%d%d",&n,&m,&tcase) != EOF) {
		int i,p,q,w;
		etot = &epool[0];
		memset(ehead,0,sizeof ehead);
		memset(vis,false,sizeof vis);
		memset(table,0,sizeof table);
		memset(first,0,sizeof first);
		for (i=1; i<=n; ++i) f[i] = i;
		for (i=1; i<=m; ++i) {
			scanf("%d%d%d",&p,&q,&w);
			link(p,q,w);
			link(q,p,w);
			merge(p,q);
		}
		static bool isroot[maxn];
		memset(isroot,false,sizeof isroot);
		for (i=1; i<=n; ++i) isroot[find(i)] = true;
		for (i=1; i<=n; ++i)
			if (isroot[i]) link(vroot,i,0), link(i,vroot,0);
		cnt = 0;
		dfs(vroot);
		rmq_init();
		while (tcase--) {
			scanf("%d%d",&p,&q);
			if (first[p] > first[q]) swap(&p,&q);
			int lca = idx[rmq(first[p],first[q])];
			if (lca == vroot) puts("Not connected"); else
				printf("%d\n",dis[p]+dis[q]-2*dis[lca]);
		}
	}
	return 0;
}
