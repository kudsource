#pragma comment(linker,"/STACK:100000000,100000000")
#include <cstdio>
#include <fstream>
#include <cctype>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=50010;

int father[maxn],dep[maxn],size[maxn],son[maxn],top[maxn],w[maxn];
int ehead[maxn],data[maxn];

int add[maxn<<2];

struct {
	int adj,next;
} edge[maxn<<1];

struct {
	int src,dest;
} map[maxn];

int tot,edge_tot;

//Implementation of Segment Tree

void tree_ins(int p, int q, int l, int r, int v, int root)
{
	if (l<=p && q<=r)
	{
		add[root]+=v;
		return;
	}
	int m=(p+q)>>1;
	if (l<=m) tree_ins(p,m,l,r,v,root<<1);
	if (r>m) tree_ins(m+1,q,l,r,v,root<<1|1);
}

int tree_query(int p, int q, int pos, int root)
{
	if (p==q) return add[root];
	int m=(p+q)>>1,tmp=add[root];
	if (pos<=m) tmp+=tree_query(p,m,pos,root<<1); else tmp+=tree_query(m+1,q,pos,root<<1|1);
	return tmp;
}

//End of Segment Tree

inline void insedge(int x, int y)
{
	edge[++edge_tot].adj=y;
	edge[edge_tot].next=ehead[x];
	ehead[x]=edge_tot;
}

void dfs(int root)
//solve father, dep, size, son
{
	size[root]=1;
	son[root]=0;
	for (int now=ehead[root]; now; now=edge[now].next)
		if (edge[now].adj!=father[root]) 
		{
			father[edge[now].adj]=root;
			dep[edge[now].adj]=dep[root]+1;
			dfs(edge[now].adj);
			size[root]+=size[edge[now].adj];
			if (size[son[root]]<size[edge[now].adj]) son[root]=edge[now].adj;
		}
}

void split(int root, int tp)
//solve top, w
{
	top[root]=tp;
	w[root]=++tot;
	if (son[root]) split(son[root],top[root]);
	for (int now=ehead[root]; now; now=edge[now].next)
		if (edge[now].adj!=father[root]  && edge[now].adj!=son[root])
			split(edge[now].adj,edge[now].adj);
}

inline int lca(int p, int q) 
{
	int t1=top[p],t2=top[q];
	while (t1!=t2)
	{
		if (dep[t1]<dep[t2]) swap(t1,t2),swap(p,q);
		p=father[t1];
		t1=top[p];
	}
	return dep[p]<dep[q] ? p : q;
}

void increase(int x, int y, int k)
//Guaranteed that y is x's ancient
//add all camp on path(x to y) with k
{
	if (top[x]==top[y])
	{
		tree_ins(1,tot,w[y],w[x],k,1);
		return;
	}
	tree_ins(1,tot,w[top[x]],w[x],k,1);
	increase(father[top[x]],y,k);
}

int main()
{
	int n,m,p;
	while (scanf("%d%d%d",&n,&m,&p)!=EOF)
	{
		for (int i=1; i<=n; i++) scanf("%d",&data[i]);
		int x,y;
		edge_tot=0;
		memset(ehead,0,sizeof(ehead));
		memset(add,0,sizeof(add));
		for (int i=1; i<n; i++)
		{
			scanf("%d%d",&x,&y);
			map[i].src=x;
			map[i].dest=y;
			insedge(x,y);
			insedge(y,x);
		}
		dep[1]=1;
		father[1]=0;
		dfs(1);
		tot=0;
		split(1,1);
		for (int i=1; i<n; i++)
			if (dep[map[i].src]>dep[map[i].dest]) swap(map[i].src,map[i].dest);
		while (p--)
		{		
			char order='\0';
			while (!isupper(order)) scanf("%c",&order);
			if (order=='Q') 
			{
				int v;
				scanf("%d",&v);
				printf("%d\n",data[v]+tree_query(1,tot,w[v],1));
			}
			else
			{
				int x,y,k;
				scanf("%d%d%d",&x,&y,&k);
				if (order=='D') k=-k;
				int tmp=lca(x,y);
				increase(x,tmp,k);
				increase(y,tmp,k);
				data[tmp]-=k;
			}
		}
	}
	return 0;
}
