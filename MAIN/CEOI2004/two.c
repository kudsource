/*
c[i] = c[i-1] + sumw[i-1] * d[i-1]
//dis[i] = sigma(j = 1..i-1,d[j])
w[i,j] = c[j] - c[i-1] - sumw[i-1] * (dis[j] - dis[i-1])

dp[i] = min{c[j] + w[j+1,i] + w[i+1,n+1]}

c[k] + w[k+1,i] < c[j] + w[j+1,i]

sumw[k] * (dis[k] - dis[i]) < sumw[j] * (dis[j] - dis[i])

sumw[k] * dis[k] - sumw[k] * dis[i] < sumw[j] * dis[j] - sumw[j] * dis[i]

y(k) = sumw[k] * dis[k]
x(k) = sumw[k]

y(k) - y(j) < dis[i] * (x(k) - x(j))

j < k ( x(j) < x(k) )

slope < d[i]

j > k ( x(j) > x(k) )

slope > d[i]
*/

#include <stdio.h>
#include <limits.h>

#define maxn 20010

int n,w[maxn],d[maxn],dis[maxn],c[maxn],queue[maxn];

inline int min(int p, int q) { return p < q ? p : q; }

inline int y(int k) { return w[k] * dis[k]; }
inline int x(int k) { return w[k]; }

inline double slope(int p, int q) {
	return (y(p) - y(q)) / (double)(x(p) - x(q));
}

inline int trans(int l, int r) {
	return c[r] - c[l-1] - w[l-1] * (dis[r] - dis[l-1]);
}

int main() {
	scanf("%d",&n);
	int i;
	for (i = 1; i <= n; ++i)
		scanf("%d%d",w+i, d+i);
	for (i = 1; i <= n; ++i) {
		w[i] += w[i-1];
		dis[i] = dis[i-1] + d[i-1];
	}
	w[n + 1] = w[n];
	dis[n + 1] = dis[n] + d[n];
	for (i = 1; i <= n+1; ++i) c[i] = c[i-1] + w[i-1] * d[i-1];
	int *qf,*qr,ans = INT_MAX;
	qf = qr = &queue[0];
	++qf;
	for (i = 1; i <= n; ++i) {
		while (qf < qr && slope(*(qr-1),*qr) >= slope(*qr,i)) --qr;
		*++qr = i;
		while (qf < qr && slope(*qf,*(qf+1)) < dis[i]) ++qf;
		ans = min(ans,c[*qf] + trans(*qf+1,i) + trans(i+1,n+1));
	}
	printf("%d\n",ans);
	return 0;
}
