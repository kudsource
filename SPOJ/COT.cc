#include <cstdio>
#include <queue>
#include <vector>
#include <algorithm>

using namespace std;

const int maxn = 1e5 + 10;

int n, m, w[maxn], rlim;
vector<int> table;

vector<int> epool[maxn];

struct node {
  node *ch[2];
  int size;
} pool[maxn << 6], *nil = pool, *tot = pool, *root[maxn];

int dp[maxn][18], dep[maxn]; //lca

node* Modify(node *orig, int l, int r, int pos) {
  node *cur = ++tot;
  *cur = *orig;
  cur->size++;
  if (l == r) return cur;
  int mid = (l + r) >> 1;
  if (pos <= mid)
    cur->ch[0] = Modify(orig->ch[0], l, mid, pos);
  else
    cur->ch[1] = Modify(orig->ch[1], mid + 1, r, pos);
  return cur;
}

void turn(vector<node*> &vec, int d) {
  vector<node*>::iterator iter;
  for (iter = vec.begin(); iter != vec.end(); ++iter)
    if (*iter) *iter = (*iter)->ch[d];
}

int calc(vector<node*> &vec) {
  int res = 0;
  vector<node*>::iterator iter;
  for (iter = vec.begin(); iter != vec.end(); ++iter)
    if (*iter && (*iter)->ch[0]) res += (*iter)->ch[0]->size;
  return res;
}

int Query(int l, int r, int k, vector<node*> &add, vector<node*> &del) {
  if (l == r) return l;
  int sw = calc(add) - calc(del);
  int mid = (l + r) >> 1;
  bool d = k > sw;
  turn(add, d);
  turn(del, d);
  if (d) {
    k -= sw;
    return Query(mid + 1, r, k, add, del);
  } else {
    return Query(l, mid, k, add, del);
  }
}

void bfs(int s) {
  queue<int> q;
  q.push(s);
  root[s] = Modify(nil, 0, rlim, w[s]);
  dp[s][0] = s;
  dep[s] = 1;
  vector<int>::iterator iter;
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    for (iter = epool[a].begin(); iter != epool[a].end(); ++iter) {
      if (!root[*iter]) {
        root[*iter] = Modify(root[a], 0, rlim, w[*iter]);
        q.push(*iter);
        dep[*iter] = dep[a] + 1;
        dp[*iter][0] = a;
      }
    }
  }
}

int lca(int u, int v) {
  if (dep[u] < dep[v]) swap(u, v);
  for (int i = 17; i >= 0; --i) {
    if (dep[dp[u][i]] >= dep[v]) {
      u = dp[u][i];
      if (dep[u] == dep[v]) break;
    }
  }
  if (u == v) return u;
  for (int i = 17; i >= 0; --i)
    if (dp[u][i] != dp[v][i])
      u = dp[u][i], v = dp[v][i];
  return dp[u][0];
}

int query(int u, int v, int k) {
  int p = lca(u, v);
  vector<node*> add, del;
  add.push_back(root[u]);
  add.push_back(root[v]);
  del.push_back(root[p]);
  if (dp[p][0] != p) del.push_back(root[dp[p][0]]);
  return table[Query(0, rlim, k, add, del)];
}

int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) scanf("%d", w + i), table.push_back(w[i]);
  sort(table.begin(), table.end());
  vector<int>::iterator iter = unique(table.begin(), table.end());
  table.erase(iter, table.end());
  rlim = table.size();
  for (int i = 1; i <= n; ++i)
    w[i] = lower_bound(table.begin(), table.end(), w[i]) - table.begin();
  int u, v, k;
  for (int i = 1; i < n; ++i) {
    scanf("%d%d", &u, &v);
    epool[u].push_back(v);
    epool[v].push_back(u);
  }
  nil->ch[0] = nil->ch[1] = nil;
  bfs(1);
  for (int j = 1; j < 18; ++j)
    for (int i = 1; i <= n; ++i)
      dp[i][j] = dp[dp[i][j-1]][j-1];
  while (m--) {
    scanf("%d%d%d", &u, &v, &k);
    printf("%d\n", query(u, v, k));
  }
  delete nil;
  return 0;
}
