#include <cstdio>
#include <cstring>
#include <algorithm>

#define rep(i,p,q) for (i=p; i<=q; ++i)

using namespace std;

const int maxn=250000+10;

char str[maxn];

struct SAM
{
	int len;
	SAM *fail,*next[26];
} sam[maxn*2],*root,*last;

int ans[maxn]={0},cnt=0,i,sum[maxn*2],queue[maxn*2],w[maxn*2];

void insert(int ch)
{
	SAM *p=last,*np=&sam[++cnt];
	np->len=last->len+1;
	last=np;
	for (; p && !p->next[ch]; p=p->fail) p->next[ch]=np;
	if (!p) np->fail=root; else
		if (p->next[ch]->len==p->len+1) np->fail=p->next[ch]; else
		{
			SAM *q=p->next[ch],*nq=&sam[++cnt];
			*nq=*q;
			nq->len=p->len+1;
			q->fail=np->fail=nq;
			for (; p && p->next[ch]==q; p=p->fail) p->next[ch]=nq;
		}
}

int main()
{
	scanf("%s",str+1);
	memset(sam,0,sizeof(sam));
	last=root=&sam[++cnt];
	int n=strlen(str+1);
	rep(i,1,n) insert(str[i]-'a');
	memset(sum,sizeof(sum),0);
	SAM *now=root;
	rep(i,1,cnt) ++sum[sam[i].len];
	rep(i,1,n) sum[i]+=sum[i-1];
	rep(i,1,cnt) queue[sum[sam[i].len]--]=i;
	rep(i,1,n) ++w[(now=now->next[str[i]-'a'])-sam];
	for (i=cnt; i; --i) 
	{
		ans[sam[queue[i]].len]=max(ans[sam[queue[i]].len],w[queue[i]]);
		if (sam[queue[i]].fail) w[sam[queue[i]].fail-sam]+=w[queue[i]];
	}
	for (i=n; i>1; --i) ans[i-1]=max(ans[i-1],ans[i]);
	rep(i,1,n) printf("%d\n",ans[i]);
	return 0;
}
