#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn = 1e5 + 10;

struct node {
  node *p, *ch[2];
  bool rev, flag;
  int val, lx, rx, mx, size, sum;
  inline bool dir() { return this == p->ch[1]; }
  inline bool isroot() { return !(this == p->ch[0] || this == p->ch[1]); }
  inline void setv(int x);
  inline void relax();
  inline void update();
} pool[maxn], *nil = pool;

inline void node::setv(int x) {
  flag = true;
  val = x;
  sum = x * size;
  lx = rx = mx = max(0, x) * size;
}

inline void node::relax() {
  if (rev) {
    swap(ch[0], ch[1]);
    swap(lx, rx);
    if (ch[0] != nil) ch[0]->rev ^= 1;
    if (ch[1] != nil) ch[1]->rev ^= 1;
    rev = false;
  }
  if (flag) {
    if (ch[0] != nil) ch[0]->setv(val);
    if (ch[1] != nil) ch[1]->setv(val);
    flag = false;
  }
}

inline void node::update() {
  if (ch[0] != nil) ch[0]->relax();
  if (ch[1] != nil) ch[1]->relax();
  size = ch[0]->size + 1 + ch[1]->size;
  sum = ch[0]->sum + val + ch[1]->sum;
  lx = max(ch[0]->lx, ch[0]->sum + val + ch[1]->lx);
  rx = max(ch[1]->rx, ch[0]->rx + val + ch[1]->sum);
  mx = max(max(ch[0]->mx, ch[1]->mx), ch[0]->rx + val + ch[1]->lx);
}

void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  int d = u->dir();
  u->p = v->p;
  if (!v->isroot()) v->p->ch[v->dir()] = u;
  if (u->ch[d^1] != nil) u->ch[d^1]->p = v;
  v->ch[d] = u->ch[d^1];
  u->ch[d^1] = v;
  v->p = u;
  v->update();
  u->update();
}

void splay(node *u, node *target = nil) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      (u->dir() == u->p->dir()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

node* expose(node *u) {
  node *v;
  for (v = nil; u != nil; u = u->p) {
    splay(u);
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void evert(node *u) {
  expose(u)->rev ^= 1;
  splay(u);
}

int main() {
  int n, op, a, b, c;
  scanf("%d", &n);
  nil->p = nil->ch[0] = nil->ch[1] = nil;
  for (int i = 1; i <= n; ++i) {
    scanf("%d", &pool[i].val);
    pool[i].ch[0] = pool[i].ch[1] = pool[i].p = nil;
    pool[i].update();
  }
  for (int i = 1; i < n; ++i) {
    scanf("%d%d", &a, &b);
    evert(pool + a);
    pool[a].p = pool + b;
  }
  int tcase;
  scanf("%d", &tcase);
  while (tcase--) {
    scanf("%d%d%d", &op, &a, &b);
    evert(pool + a);
    switch (op) {
      case 1:
        printf("%d\n", expose(pool + b)->mx);
        break;
      case 2:
        scanf("%d", &c);
        expose(pool + b)->setv(c);
        break;
    }
  }
  return 0;
}
