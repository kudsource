{$inline on}

uses math;

const
  maxn=510;
  maxm=10000;

var
  e:array [0..maxm] of record
                         adj,next,cap:longint;
                       end;
  l:array [0..maxn] of record
                         p,num:longint;
                       end;
  data:array [0..maxm] of record
                            s,t:longint;
                          end;
  dis,h,ans:array [0..maxn] of longint;
  v:array [0..maxn] of boolean;
  n,m,maxl,now,cnt,t,k:longint;

procedure init; inline;
var
  i:longint;

begin
  fillchar(l,sizeof(l),0);
  fillchar(ans,sizeof(ans),0);
  readln(n,m);
  for i:=1 to m do
    with data[i] do readln(s,t);
  readln(k);
  maxl:=0;
  for i:=1 to k do
    begin
      readln(l[i].p,l[i].num);
      maxl:=max(maxl,l[i].num);
    end;
end;

procedure link(p,q,c:longint); inline;
begin
  e[cnt].adj:=q;
  e[cnt].next:=h[p];
  e[cnt].cap:=c;
  h[p]:=cnt;
  inc(cnt);
end;

procedure build(tmp:longint); inline;
var
  i:longint;

begin
  fillchar(h,sizeof(h),255);
  cnt:=0;
  for i:=1 to m do
    with data[i] do
      begin
        link(s,t,1);
        link(t,s,1);
      end;
  for i:=1 to k do
    with l[i] do
      if num and (1 shl tmp)>0 then
        begin
          link(0,p,maxint);
          link(p,0,0);
        end
      else
        begin
          link(p,n+1,maxint);
          link(n+1,p,0);
        end;
end;

function bfs(s,t:longint):boolean;
var
  q:array [0..maxn] of longint;
  qf,qr:^longint;
  i,p,j,w:longint;

begin
  fillchar(dis,sizeof(dis),255);
  qf:=@q[0];
  qr:=@q[1];
  qr^:=s;
  dis[s]:=0;
  repeat
    inc(qf);
    p:=h[qf^];
    while p<>-1 do
      begin
        j:=e[p].adj;
        w:=e[p].cap;
        if (w>0) and (dis[j]=-1) then
          begin
            inc(qr);
            qr^:=j;
            dis[j]:=dis[qf^]+1;
            if j=t then exit(true);
          end;
        p:=e[p].next;
      end;
  until qf>=qr;
  exit(false);
end;

function dfs(now,low:longint):longint;
var
  i,p,w,tmp,sum:longint;

begin
  if now=n+1 then exit(low);
  p:=h[now];
  sum:=0;
  while p<>-1 do
    begin
      i:=e[p].adj;
      w:=e[p].cap;
      if (w>0) and (dis[i]=dis[now]+1) and (sum<low) then
        begin
          tmp:=dfs(i,min(low-sum,w));
          if tmp>0 then
            begin
              dec(e[p].cap,tmp);
              inc(e[p xor 1].cap,tmp);
              inc(sum,tmp);
            end;
        end;
      p:=e[p].next;
    end;
  if sum=0 then dis[now]:=-1;
  exit(sum);
end;

function dinic(s,t:longint):longint;
var
  tmp,ans:longint;

begin
  dinic:=0;
  while bfs(s,t) do
    repeat
      tmp:=dfs(s,maxint);
      inc(dinic,tmp);
    until tmp=0;
end;

procedure search(k,now:longint);
var
  p,i:longint;

begin
  ans[k]:=ans[k] or (1 shl now);
  p:=h[k];
  v[k]:=true;
  while p<>-1 do
    begin
      i:=e[p].adj;
      if (not v[i]) and (e[p].cap>0) then search(i,now);
      p:=e[p].next;
    end;
end;

procedure solve;
var
  i:longint;

begin
  for i:=0 to 31 do
    begin
      if 1 shl i>maxl then break;
      build(i);
      dinic(0,n+1);
      fillchar(v,sizeof(v),false);
      search(0,i);
    end;
  for i:=1 to n do writeln(ans[i]);
end;

begin
  readln(t);
  repeat
    dec(t);
    init;
    solve;
  until t=0;
end.
