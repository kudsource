#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=250000+10;

struct SAM
{
	int len;
	SAM *pre,*next[26];
	SAM(): pre(NULL), len(0) { memset(next,0,sizeof(next)); }
} sam[maxn*4],*root,*last;

int cnt=0;

namespace suffix_automation
{
	void insert(int ch) //ch==ASCII code - 'a'
	{
		SAM *p=last,*np=&sam[cnt++];
		np->len=last->len+1;
		for (; p && !p->next[ch]; p=p->pre) p->next[ch]=np;
		last=np;
		if (!p) np->pre=root; else
			if (p->next[ch]->len==p->len+1) np->pre=p->next[ch]; else
			{
				SAM *q=p->next[ch],*nq=&sam[cnt++];
				*nq=*q;
				nq->len=p->len+1;
				q->pre=np->pre=nq;
				for (; p && p->next[ch]==q; p=p->pre) p->next[ch]=nq;
			}
	}

	int solve(char s[])
	{
		int n=strlen(s),res=0,tmp=0;
		SAM *now=root;
		for (int i=0; i<n; ++i)
		{
			int ch=s[i]-'a';
			if (now->next[ch]) ++tmp,now=now->next[ch]; else
			{
				while (now && !now->next[ch]) now=now->pre;
				if (!now) now=root,tmp=0; else tmp=now->len+1,now=now->next[ch];
			}
			res=max(res,tmp);
		}
		return res;
	}
}

char a[maxn],b[maxn];

int main()
{
	last=root=&sam[cnt++];
	scanf("%s %s",a,b);
	int n=strlen(a);
	for (int i=0; i<n; ++i) suffix_automation::insert(a[i]-'a');
	printf("%d\n",suffix_automation::solve(b));
}
