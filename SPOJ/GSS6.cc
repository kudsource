#include <cstdio>
#include <cctype>
#include <algorithm>

using namespace std;

const int maxn = 2e5 + 10, inf = 1 << 25;

struct node {
  node *p, *ch[2];
  int size;
  int lx, rx, mx, sum, val;
  inline bool dir() { return this == p->ch[1]; }
  inline void setc(node*, int);
  inline void update();
};

node pool[maxn];
node *nil = pool, *tot = nil + 1, *root;

inline void node::setc(node *c, int d) {
  ch[d] = c;
  c->p = this;
}

inline void node::update() {
  size = ch[0]->size + 1 + ch[1]->size;
  sum = ch[0]->sum + val + ch[1]->sum;
  lx = max(ch[0]->lx, ch[0]->sum + val + max(ch[1]->lx, 0));
  rx = max(max(ch[0]->rx, 0) + val + ch[1]->sum, ch[1]->rx);
  mx = max(max(ch[0]->mx, ch[1]->mx), max(ch[0]->rx, 0) + val + max(ch[1]->lx, 0));
} 

inline node* makenode(int x) {
  tot->ch[0] = tot->ch[1] = tot->p = nil;
  tot->size = 1;
  tot->lx = tot->rx = tot->mx = tot->sum = tot->val = x;
  return tot++;
}

inline void rotate(node *u) {
  node *v = u->p;
  int d = u->dir();
  v->p->setc(u, v->dir());
  v->setc(u->ch[d^1], d);
  u->setc(v, d^1);
  v->update();
  u->update();
  if (v == root) root = u;
}

inline void splay(node *u, node *target = nil) {
  while (u->p != target) {
    if (u->p->p == target)
      rotate(u);
    else
      (u->dir() == u->p->dir()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

inline node* select(int pos) {
  node *u = root;
  for (;;) {
    int k = u->ch[0]->size;
    if (k == pos) break;
    if (k < pos) {
      pos -= (k + 1);
      u = u->ch[1];
    } else {
      u = u->ch[0];
    }
  }
  splay(u);
  return u;
}

int n;

inline void init() {
  nil->ch[0] = nil->ch[1] = nil->p = nil;
  nil->mx = nil->lx = nil->rx = nil->val = -inf;
  root = makenode(-inf);
  node *u = root, *v;
  for (int i = 1, x; i <= n; ++i) {
    scanf("%d", &x);
    v = makenode(x);
    u->setc(v, 1);
    u = v;
  }
  v = makenode(-inf);
  u->setc(v, 1);
  splay(v);
}

inline void ins(int pos, int val) {
  node *u = select(pos - 1), *v = makenode(val);
  splay(u);
  v->setc(u->ch[1], 1);
  u->setc(v, 1);
  splay(v);
}

inline void del(int pos) {
  node *u = select(pos - 1), *v = select(pos + 1);
  splay(u);
  splay(v, u);
  v->setc(nil, 0);
  splay(v);
}

inline void replace(int pos, int val) {
  node *u = select(pos);
  splay(u);
  u->val = val;
  u->update();
}

inline int query(int l, int r) {
  node *u = select(l - 1), *v = select(r + 1);
  splay(u);
  splay(v, u);
  return v->ch[0]->mx;
}

int main() {
  scanf("%d", &n);
  init();
  int tcase;
  scanf("%d", &tcase);
  char op;
  int a, b;
  while (tcase--) {
    scanf(" %c%d", &op, &a);
    switch (op) {
      case 'D':
        del(a);
        break;
      default:
        scanf("%d", &b);
        switch(op) {
          case 'I':
            ins(a, b);
            break;
          case 'Q':
            printf("%d\n", query(a, b));
            break;
          case 'R':
            replace(a, b);
            break;
        }
        break;
    }
  }
  return 0;
}
