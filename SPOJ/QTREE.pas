const
  maxn=20100;

var
  n,tot,edge_tot,p,q,t,i:longint;
  dep,top,father,son,w,size,ehead:array [0..maxn] of longint;
  edge:array [0..maxn*2] of record
                              adj,next:longint;
                            end;
  tree:array [0..maxn*4] of longint;
  data:array [0..maxn] of record
                            x,y,v:longint;
                          end;
  order,ch:char;

//Implementation of Segment Tree

function max(p,q:longint):longint;
begin
  if p>q then exit(p) else exit(q);
end;

procedure update(root:longint);
begin
  tree[root]:=max(tree[root*2],tree[root*2+1]);
end;

procedure tree_ins(l,r,pos,v,root:longint);
var
  m:longint;

begin
  if l=r then
    begin
      tree[root]:=v;
      exit;
    end;
  m:=(l+r) shr 1;
  if pos<=m then tree_ins(l,m,pos,v,root*2) else
    tree_ins(m+1,r,pos,v,root*2+1);
  update(root);
end;

function tree_query(l,r,p,q,root:longint):longint;
var
  m,t:longint;

begin
  if (p<=l) and (r<=q) then exit(tree[root]);
  m:=(l+r) shr 1;
  t:=0;
  if p<=m then t:=max(t,tree_query(l,m,p,q,root*2));
  if q>m then t:=max(t,tree_query(m+1,r,p,q,root*2+1));
  exit(t);
end;

//End of Segment Tree

procedure insedge(x,y:longint);
begin
  inc(edge_tot);
  edge[edge_tot].adj:=y;
  edge[edge_tot].next:=ehead[x];
  ehead[x]:=edge_tot;
end;

procedure dfs(root:longint);
//solve father,dep,size,son
var
  now:longint;

begin
  size[root]:=1;
  son[root]:=0;
  now:=ehead[root];
  while now<>-1 do
    begin
      if edge[now].adj<>father[root] then
        begin
          father[edge[now].adj]:=root;
          dep[edge[now].adj]:=dep[root]+1;
          dfs(edge[now].adj);
          inc(size[root],size[edge[now].adj]);
          if size[son[root]]<size[edge[now].adj] then son[root]:=edge[now].adj;
        end;
      now:=edge[now].next;
    end;
end;

procedure split(root,tp:longint);
//solve top,w
var
  now:longint;

begin
  inc(tot);
  w[root]:=tot;
  top[root]:=tp;
  if son[root]>0 then split(son[root],top[root]);
  now:=ehead[root];
  while now<>-1 do
    begin
      if (edge[now].adj<>father[root]) and (edge[now].adj<>son[root]) then
        split(edge[now].adj,edge[now].adj);
      now:=edge[now].next;
    end;
end;

procedure swap(var p,q:longint);
var
  t:longint;

begin
  t:=p;
  p:=q;
  q:=t;
end;

function find(p,q:longint):longint;
var
  t1,t2,tmp:longint;

begin
  t1:=top[p];
  t2:=top[q];
  tmp:=0;
  while t1<>t2 do
    begin
      if dep[t1]<dep[t2] then
        begin
          swap(t1,t2);
          swap(p,q);
        end;
      tmp:=max(tmp,tree_query(1,tot,w[t1],w[p],1));
      p:=father[t1];
      t1:=top[p];
    end;
  if p=q then exit(tmp);
  if dep[p]>dep[q] then swap(p,q);
  exit(max(tmp,tree_query(1,tot,w[son[p]],w[q],1)));
end;

begin
  readln(t);
  repeat
    dec(t);
    readln(n);
    edge_tot:=0;
    fillchar(ehead,sizeof(ehead),255);
    fillchar(tree,sizeof(tree),0);
    for i:=1 to n-1 do
      begin
        readln(data[i].x,data[i].y,data[i].v);
        insedge(data[i].x,data[i].y);
        insedge(data[i].y,data[i].x);
      end;
    dep[1]:=0;
    father[1]:=0;
    dfs(1);
    tot:=0;
    split(1,1);
    for i:=1 to n-1 do
      begin
        if dep[data[i].x]>dep[data[i].y] then swap(data[i].x,data[i].y);
        tree_ins(1,tot,w[data[i].y],data[i].v,1);
      end;
    read(order);
    while order<>'D' do
      begin
        repeat read(ch); until ch=' ';
        readln(p,q);
        if order='Q' then writeln(find(p,q)) else
          tree_ins(1,tot,w[data[p].y],q,1);
        read(order);
      end;
    repeat read(ch); until (ch=#10) or (ch=#13);
  until t=0;
end.
