#include <cstdio>
#include <cstring>
#include <limits>
#include <algorithm>

using std::scanf;
using std::printf;
using std::min;
using std::numeric_limits;

typedef long long int64;

const int maxn = 5010, maxm = 60010;

int n, m;

int adj[maxn];
int next[maxm], to[maxm], cap[maxm], etot = 0;

void link(int a, int b, int c) {
  to[etot] = b;
  next[etot] = adj[a];
  cap[etot] = c;
  adj[a] = etot++;
  to[etot] = a;
  next[etot] = adj[b];
  cap[etot] = c;
  adj[b] = etot++;
}

int h[maxn], gap[maxn];

int dfs(int a, int df, const int s, const int t) {
  if (a == t) return df;
  for (int i = adj[a]; i != -1; i = next[i]) {
    int b = to[i], c = cap[i];
    if (h[a] == h[b] + 1 && c) {
      int f = dfs(b, min(c, df), s, t);
      if (f) {
        cap[i] -= f;
        cap[i^1] += f;
        return f;
      }
    }
  }
  if (--gap[h[a]] == 0) h[s] = n;
  ++gap[++h[a]];
  return 0;
}

int64 isap(const int s, const int t) {
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  int64 res = 0;
  while (h[s] < n) res += dfs(s, numeric_limits<int>::max(), s, t);
  return res;
}

int main() {
  scanf("%d%d", &n, &m);
  memset(adj, -1, sizeof adj);
  int a, b, c;
  while (m--) {
    scanf("%d%d%d", &a, &b, &c);
    link(a, b, c);
  }
  printf("%lld\n", isap(1, n));
  return 0;
}
