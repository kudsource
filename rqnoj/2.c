#include <stdio.h>

#define max_value 50010
#define maxn 25

int max(int p, int q) {
	return p>q?p:q;
}

int main() {
	int money,n;
	scanf("%d%d",&money,&n);
	int f[max_value]={0},value[maxn],price[maxn];
	int i,j;
	for (i=1; i<=n; i++) scanf("%d%d",&(price[i]),&value[i]),value[i]*=price[i];
	for (i=1; i<=n; i++) 
		for (j=money; j>=price[i]; j--) 
			f[j]=max(f[j],f[j-price[i]]+value[i]);
	int ans=0;
	for (i=0; i<=money; i++) ans=max(ans,f[i]);
	printf("%d\n",ans);
	return 0;
}