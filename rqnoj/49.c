#include <stdio.h>

#define maxn 35

int score[maxn],opt[maxn][maxn],root[maxn][maxn];

int max(int p, int q)
{
	return p>q?p:q;
}

int mdfs(int l, int r) 
{
	int i;
	if (opt[l][r]) return opt[l][r];
	if (l==r) return score[l];
	if (l>r) return 1;
	int tmp=0;
	for (i=l; i<=r; i++) 
	{
		opt[l][i-1]=mdfs(l,i-1);
		opt[i+1][r]=mdfs(i+1,r);
		tmp=opt[l][i-1]*opt[i+1][r]+score[i];
		if (tmp>opt[l][r]) 
		{
			opt[l][r]=tmp;
			root[l][r]=i;
		}
	}
	return opt[l][r];
}

void printroot(int l, int r)
{
	if (l>r) return;
	printf("%d ",root[l][r]);
	printroot(l,root[l][r]-1);
	printroot(root[l][r]+1,r);
}

int main()
{
	int n,i,j;
	scanf("%d",&n);
	for (i=1; i<=n; i++) 
	{
		scanf("%d",&(score[i]));
		opt[i][i]=score[i];
		root[i][i]=i;
	}
	printf("%d\n",mdfs(1,n));
	printroot(1,n);
	printf("\n");
	return 0;
}