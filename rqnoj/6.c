#include <stdio.h>

#define max_value 200010
#define maxn 100

int max(int p, int q) {
	return p>q?p:q;
}

int main() {
	int money,n;
	scanf("%d%d",&money,&n);
	money/=10;
	int f[max_value]={0},v[maxn],p[maxn],q[maxn],q1[maxn],q2[maxn];
	int i,j;
	for (i=1; i<=n; i++) {
		scanf("%d%d%d",&(v[i]),&(p[i]),&(q[i]));
		v[i]/=10;
		q2[q[i]]=q1[q[i]];
		q1[q[i]]=i;
	}
	f[0]=1;
	int ans=0;
	for (j=1; j<=n; j++) 
		if (q[j]==0)
			for (i=money; i>=v[j]; i--) {
				if (i-v[j]>=0) f[i]=max(f[i],f[i-v[j]]+v[j]*p[j]);
				if (i-v[j]-v[q1[j]]>=0) f[i]=max(f[i],f[i-v[j]-v[q1[j]]]+v[j]*p[j]+v[q1[j]]*p[q1[j]]);
				if (i-v[j]-v[q2[j]]>=0) f[i]=max(f[i],f[i-v[j]-v[q2[j]]]+v[j]*p[j]+v[q2[j]]*p[q2[j]]);
				if (i-v[j]-v[q1[j]]-v[q2[j]]>=0) f[i]=max(f[i],f[i-v[j]-v[q1[j]]-v[q2[j]]]+v[j]*p[j]+v[q1[j]]*p[q1[j]]+v[q2[j]]*p[q2[j]]);
				ans=max(ans,f[i]);
			}
	printf("%d\n",(--ans)*10);
	return 0;
}