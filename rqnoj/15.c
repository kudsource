#include <stdio.h>

#define maxn 110
#define max_time 1010

int max(int p, int q) {
	return p>q?p:q;
}

int main() {
	int total_time,n;
	scanf("%d%d",&total_time,&n);
	int i,j,time[maxn],value[maxn],f[max_time]={0};
	for (i=1; i<=n; i++) scanf("%d%d",&(time[i]),&(value[i]));
	for (i=1; i<=n; i++) 
		for (j=total_time; j>=time[i]; j--) 
			f[j]=max(f[j],f[j-time[i]]+value[i]);
	int ans=0;
	for (i=0; i<=total_time; i++) ans=max(ans,f[i]);
	printf("%d\n",ans);
	return 0;
}