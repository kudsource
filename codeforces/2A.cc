#include <climits>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <utility>

using namespace std;

int main()
{
	int n,i,score;
	string name;
	cin>>n;
	map<string,int> table;
	vector< pair<string,int> > data;
	for (i=0; i<n; ++i)
	{
		cin>>name>>score;
		data.push_back(pair<string,int>(name,score));
	}
	for (i=0; i<n; ++i)
	{
		table[data[i].first]+=data[i].second;
		data[i].second=table[data[i].first];
	}
	int ans=INT_MIN;
	for (map<string,int>::iterator iter=table.begin(); iter!=table.end(); ++iter)
		ans=max(ans,iter->second);
	for (i=0; i<n; ++i)
		if (table[data[i].first]==ans && data[i].second>=ans) break;
	cout<<data[i].first<<endl;
	return 0;
}
