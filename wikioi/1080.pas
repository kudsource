{$inline on}

const
  maxn=100000;

var
  n,m,k,i,a,b,order:longint;
  tree:array [0..maxn] of longint;

function lowbit(k:longint):longint; inline;
begin
  exit(k and (k xor (k-1)));
end;

procedure ins(k,pos:longint); inline;
begin
  while pos<=n do
    begin
      inc(tree[pos],k);
      inc(pos,lowbit(pos));
    end;
end;

function query(p:longint):longint; inline;
begin
  query:=0;
  while p>0 do
    begin
      inc(query,tree[p]);
      dec(p,lowbit(p));
    end;
end;

begin
  readln(n);
  for i:=1 to n do
    begin
      read(k);
      ins(k,i);
    end;
  readln(m);
  for i:=1 to m do
    begin
      readln(order,a,b);
      case order of
        1:ins(b,a);
        2:writeln(query(b)-query(a-1));
      end;
    end;
end.