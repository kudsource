#include <cstdio>
#include <cstring>
#include <cmath>
#include <complex>
#include <algorithm>

using std::scanf;
using std::printf;
using std::swap;

typedef std::complex<double> complex; 

const int maxn=400001;

complex x1[maxn],x2[maxn];

void binary_reverse(complex *y, int l)
{
	register int i,j,k;
	for (i=1,j=l/2; i<l-1; ++i)
	{
		if (i<j) swap(y[i],y[j]);
		k=l/2;
		while (j>=k)
		{
			j-=k;
			k/=2;
		}
		if (j<k) j+=k;
	}
}

void FFT(complex *y, int l, double on)
{
	register int h,i,j,k;
	complex u,t;
	binary_reverse(y,l);
	for (h=2; h<=l; h<<=1)
	{
		complex wn(cos(on*2*M_PI/h),sin(on*2*M_PI/h));
		for (j=0; j<l; j+=h)
		{
			complex w(1,0);
			for (k=j; k<j+h/2; ++k)
			{
				u=y[k];
				t=w*y[k+h/2];
				y[k]=u+t;
				y[k+h/2]=u-t;
				w=w*wn;
			}
		}
	}
	if (on==-1) for (i=0; i<l; ++i) y[i]/=complex(l,0);
}

int main()
{
	char a[maxn],b[maxn];
	scanf("%s%s",a,b);
	register int i;
	int l1=strlen(a),l2=strlen(b),l=1;
	while (l<l1*2 || l<l2*2) l<<=1;
	for (i=0; i<l1; ++i) x1[i]=complex(a[l1-1-i]-'0',0);
	for (i=0; i<l2; ++i) x2[i]=complex(b[l2-1-i]-'0',0);
	FFT(x1,l,1);
	FFT(x2,l,1);
	for (i=0; i<l; ++i) x1[i]*=x2[i];
	FFT(x1,l,-1);
	int sum[maxn];
	for (i=0; i<l; ++i) sum[i]=round(x1[i].real());
	for (i=0; i<l; ++i) sum[i+1]+=sum[i]/10,sum[i]%=10;
	l=l1+l2-1;
	while (sum[l]==0 && l) --l;
	for (i=l; i>=0; --i) putchar(sum[i]+'0');
	puts("");
	return 0;
}
