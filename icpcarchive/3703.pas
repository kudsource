{$R+,S+,Q+,H+,O-}
{$MINSTACKSIZE 4000000}

var
  n,i,ans,count:longint;
  a,b,c:array [0..100] of string; 
  pre,plan:array [0..5000000] of string;

function getstr:string;
var
  ch:char;

begin
  repeat
    read(ch);
  until ch>' ';
  getstr:='';
  repeat
    getstr:=getstr+ch;
    read(ch);
  until ch<=' ';
end;

procedure trans(var a,b:string);
var
  i:longint;

begin
  b:=copy(a,1,length(a)-length(b))+b;
  a:='6'+a;
  while length(a)<12 do a:=a+'0';
  for i:=length(a) downto 1 do
    if a[i]='0' then a[i]:='9' else
      begin
        a[i]:=pred(a[i]);
        break;
      end;
  b:='6'+b;
  while length(b)<12 do b:=b+'9';
  for i:=length(b) downto 1 do
    if b[i]='9' then b[i]:='0' else
      begin
        b[i]:=succ(b[i]);
        break;
      end;
end;

function cal(s:string):string;
var
  i:longint;

begin
  cal:='@';
  for i:=1 to n do
    begin
      if (copy(a[i],1,length(s))=s) and (length(s)<12) then exit;
      if (copy(b[i],1,length(s))=s) and (length(s)<12) then exit;
      if (a[i]<s) and (s<b[i]) then exit(c[i]);
    end;
  cal:='invalid';
end;

function build(prefix:string):string;
var
  child:array ['0'..'9'] of string;
  i:char;

begin
  fillchar(child,sizeof(child),0);
  if length(prefix)>1 then
    begin
      build:=cal(prefix);
      if build<>'@' then exit;
    end;
  build:='';
  for i:='0' to '9' do
    begin
      child[i]:=build(prefix+i);
      if build='' then build:=child[i] else if build<>child[i] then build:='@';
    end;
  if (build='@') or (length(prefix)=1) then
    begin
      for i:='0' to '9' do
        if (child[i]<>'@') and (child[i]<>'invalid') then
          begin
            inc(ans);
            pre[ans]:=prefix+i;
            plan[ans]:=child[i];
          end;
    end;
end;

procedure swap(var x,y:string); inline;
var
  t:string;

begin
  t:=x;
  x:=y;
  y:=t;
end;

procedure sort(l,r:longint);
var
  i,j:longint;
  m,t:string;

begin
  i:=l;
  j:=r;
  m:=pre[(l+r) shr 1];
  repeat
    while pre[i]<m do inc(i);
    while pre[j]>m do dec(j);
    if i<=j then
      begin
        swap(pre[i],pre[j]);
        swap(plan[i],plan[j]);
        inc(i);
        dec(j);
      end;
  until i>j;
  if l<j then sort(l,j);
  if r>i then sort(i,r);
end;

begin
  while not eof do
    begin
      inc(count);
      readln(n);
      ans:=0;
      for i:=1 to n do
        begin
          a[i]:=getstr;
          getstr;
          b[i]:=getstr;
          c[i]:=getstr;
          trans(a[i],b[i]);
         end;
      readln;
      build('6');
      if count>1 then writeln;
      writeln(ans);
      if ans>1 then sort(1,ans);
      for i:=1 to ans do writeln(copy(pre[i],2,1000),' ',plan[i]);
    end;
end.