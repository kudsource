const
  maxn=100;

var
  n,m,src,dest,cnt,i,min,ans,p,q,c:longint;
  pre:array [0..maxn] of longint;
  a:array [0..maxn,0..maxn] of longint;

function bfs(src,dest:longint):boolean;
var
  queue:array [0..maxn] of longint;
  vis:array [0..maxn] of boolean;
  head,tail,i,j:longint;

begin
  fillchar(queue,sizeof(queue),0);
  fillchar(vis,sizeof(vis),false);
  fillchar(pre,sizeof(pre),0);
  head:=0;
  tail:=1;
  queue[1]:=src;
  repeat
    inc(head);
    i:=queue[head];
    for j:=1 to n do
      if (not vis[j]) and (a[i,j]>0) then
        begin
          inc(tail);
          queue[tail]:=j;
          pre[j]:=i;
          vis[j]:=true;
          if j=dest then exit(true);
        end;
  until head>=tail;
  exit(false);
end;

begin
  cnt:=0;
  readln(n);
  while n>0 do
    begin
      readln(src,dest,m);
      inc(cnt);
      fillchar(a,sizeof(a),0);
      for i:=1 to m do
        begin
          readln(p,q,c);
          inc(a[p,q],c);
          inc(a[q,p],c);
        end;
      ans:=0;
      repeat
        if not bfs(src,dest) then break;
        i:=dest;
        min:=maxlongint;
        while i<>src do
          begin
            if a[pre[i],i]<min then min:=a[pre[i],i];
            i:=pre[i];
          end;
        inc(ans,min);
        i:=dest;
        while i<>src do
          begin
            dec(a[pre[i],i],min);
            inc(a[i,pre[i]],min);
            i:=pre[i];
          end;
      until false;
      writeln('Network ',cnt);
      writeln('The bandwidth is ',ans,'.');
      writeln;
      readln(n);
    end;
end.
