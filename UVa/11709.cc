#include <cstring>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

const int maxn = 1000 + 10, maxm = maxn*maxn;

std::pair<int,int> e[maxm];

std::map<std::string,int> table;
std::vector<int> epool[maxn];

int cnt,tot,dfn[maxn],low[maxn],sccno[maxn];

int stack[maxn],*top;
bool instack[maxn];

void tarjan(int i) {
	dfn[i] = low[i] = ++tot;
	*top++ = i, instack[i] = true;
	for (std::vector<int>::iterator p = epool[i].begin(); p != epool[i].end(); ++p) {
		if (!dfn[*p]) tarjan(*p);
		if (instack[*p]) low[i] = std::min(low[i],low[*p]);
	}
	if (dfn[i] == low[i]) {
		int j;
		++cnt;
		do {
			j = *--top;
			instack[j] = false;
			sccno[j] = cnt;
		} while (j != i);
	}
}

int main() {
	int n,m;
	while (std::cin>>n>>m, n | m) {
		table.clear();
		for (int i = 1; i <= n; ++i) epool[i].clear();
		std::string p,q;
		std::getline(std::cin,p);
		for (int i = 1; i <= n; ++i) {
			std::getline(std::cin,p);
			table[p] = i;
		}
		for (int i = 1; i <= m; ++i) {
			std::getline(std::cin,p);
			std::getline(std::cin,q);
			epool[table[p]].push_back(table[q]);
		}
		cnt = tot = 0;
		top = &stack[0];
		std::memset(dfn,0,sizeof dfn);
		std::memset(stack,0,sizeof stack);
		std::memset(instack,false,sizeof instack);
		std::memset(sccno,0,sizeof sccno);
		for (int i = 1; i <= n; ++i)
			if (!dfn[i]) tarjan(i);
		std::cout<<cnt<<std::endl;
	}
	return 0;
}
