#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define maxn 2010
#define maxm 4000010
#define min(p,q) ((p)<(q)?(p):(q))

typedef struct _edge {
	int adj;
	struct _edge *next;
} edge;

edge epool[maxm],*ehead[maxn],*cnt;

void link(int p, int q) {
	cnt->adj = q;
	cnt->next = ehead[p];
	ehead[p] = cnt++;
}

bool instack[maxn];
int dis[maxn],low[maxn],stack[maxn],tot,*top;
int sccno[maxn],scc_cnt;

void tarjan(int i) {
	dis[i] = low[i] = ++tot;

	*top++ = i;
	instack[i] = true;

	edge *p;
	for (p = ehead[i]; p != NULL; p = p->next) {
		int j = p->adj;
		if (!dis[j]) tarjan(j);
		if (instack[j]) low[i] = min(low[i],low[j]);
	}

	if (dis[i] == low[i]) {
		int j;
		++scc_cnt;
		do {
			j = *--top;
			instack[j] = false;
			sccno[j] = scc_cnt;
		} while (j != i);
	}
}

int main() {
	int tcase,n,m,i;
	while (scanf("%d%d",&n,&m), n | m) {
		cnt = &epool[0];
		scc_cnt = 0;
		memset(ehead,NULL,sizeof ehead);
		memset(epool,0,sizeof epool);
		memset(stack,0,sizeof stack);
		memset(instack,false,sizeof instack);
		memset(sccno,0,sizeof sccno);
		memset(dis,0,sizeof dis);
		int x,y,sw;
		for (i = 1; i <= m; ++i) {
			scanf("%d%d%d",&x,&y,&sw);
			link(x,y);
			if (sw == 2) link(y,x);
		}
		tot = 0;
		top = &stack[0];
		for (i = 1; i <= n; ++i) if (!dis[i]) tarjan(i);
		printf("%d\n",scc_cnt == 1);
	}
	return 0;
}
