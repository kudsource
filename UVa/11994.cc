#include <cstdio>

using namespace std;

const int maxn = 5e4 + 10;

struct node {
  node *ch[2], *p;
  int mask, color, size;
  bool flag;
  inline bool dir() { return this == p->ch[1]; }
  inline bool isroot() { return !(this == p->ch[0] || this == p->ch[1]); }
  inline void apply(int x);
  inline void relax();
  inline void update();
} pool[maxn], *nil = pool;

inline void node::apply(int x) {
  if (this != nil) {
    mask = color = x;
    flag = true;
  }
}

inline void node::relax() {
  if (flag) {
    ch[0]->apply(mask);
    ch[1]->apply(mask);
    flag = false;
  }
}

inline void node::update() {
  mask = ch[0]->mask | ch[1]->mask | color;
  size = ch[0]->size + ch[1]->size + 1;
}

void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  bool d = u->dir();
  u->p = v->p;
  if (!v->isroot()) v->p->ch[v->dir()] = u;
  if (u->ch[d^1] != nil) u->ch[d^1]->p = v;
  v->p = u;
  v->ch[d] = u->ch[d^1];
  u->ch[d^1] = v;
  v->update();
  u->update();
}

void splay(node *u) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      (u->dir() == u->p->dir()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

node* expose(node *u) {
  node *v;
  for (v = nil; u != nil; u = u->p) {
    splay(u);
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

void makenode(node *u) {
  u->ch[0] = u->ch[1] = u->p = nil;
  u->mask = u->flag = 0;
  u->size = 1;
}

inline node* root(node *u) {
  for (u = expose(u); u->ch[0] != nil; u = u->ch[0]) {}
  return u;
}

inline bool sametree(node *u, node *v) {
  return root(u) == root(v);
}

inline int lowbit(int x) { return x & -x; }

int bitcount(int x) {
  int res = 0;
  while (x) {
    ++res;
    x -= lowbit(x);
  }
  return res;
}

inline node* lca(node *u, node *v) {
  expose(u);
  return expose(v);
}

void change(node *u, node *fa, int c) {
  if (u == fa || (sametree(u, fa) && lca(u, fa) == u)) return;
  expose(u);
  splay(u);
  u->ch[0]->p = nil;
  u->ch[0] = nil;
  u->p = fa;
  u->color = c;
  u->update();
  expose(u);
}

void paint(node *u, node *v, int c) {
  if (!sametree(u, v)) return;
  node *p = lca(u, v);
  splay(p);
  splay(u);
  p->ch[1]->apply(c);
  if (u != p) u->apply(c);
}

void query(node *u, node *v) {
  if (u == v || !sametree(u, v)) {
    puts("0 0");
    return;
  }
  node *p = lca(u, v);
  splay(p);
  splay(u);
  int col = p->ch[1]->mask, sz = p->ch[1]->size;
  if (u != p) col |= u->mask, sz += u->size;
  printf("%d %d\n", sz, bitcount(col));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int n, m;
  static int f[maxn], c[maxn];
  nil->p = nil->ch[0] = nil->ch[1] = nil;
  while (scanf("%d%d", &n, &m) != EOF) {
    for (int i = 1; i <= n; ++i) scanf("%d", f + i);
    for (int i = 1; i <= n; ++i) {
      scanf("%d", c + i);
      c[i] = 1 << c[i];
    }
    for (int i = 1; i <= n; ++i) {
      makenode(pool + i);
      if (f[i]) {
        pool[i].p = pool + f[i];
        pool[i].color = pool[i].mask = c[i];
      }
    }
    int op, x, y, t;
    while (m--) {
      scanf("%d%d%d", &op, &x, &y);
      switch (op) {
        case 1:
          scanf("%d", &t);
          t = 1 << t;
          change(pool + x, pool + y, t);
          break;
        case 2:
          scanf("%d", &t);
          t = 1 << t;
          paint(pool + x, pool + y, t);
          break;
        case 3:
          query(pool + x, pool + y);
          break;
      }
    }
  }
  return 0;
}
