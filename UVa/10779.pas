{$inline on}

const
  maxpoint=40;
  src=1;
  dest=40;

var
  t,p,ans,min,i:longint;
  a:array [0..maxpoint,0..maxpoint] of longint;
  pre:array [0..maxpoint] of longint;

procedure init;
var
  i,j,n,m,k:longint;
  cnt:array [0..25] of longint;

begin
  readln(n,m);
  for i:=1 to n do
    begin
      read(k);
      fillchar(cnt,sizeof(cnt),0);
      for j:=1 to k do
        begin
          read(t);
          inc(a[i,t+10]);
        end;
      if i>1 then
        for j:=1 to m do
          if a[i,j+10]>0 then dec(a[i,j+10]) else if a[i,j+10]=0 then inc(a[j+10,i]);
    end;
  for i:=1 to m do a[i+10,dest]:=1;
end;

function bfs(src,dest:longint):boolean;
var
  queue:array [0..maxpoint] of longint;
  vis:array [0..maxpoint] of boolean;
  i,j,head,tail:longint;

begin
  head:=0;
  tail:=1;
  fillchar(queue,sizeof(queue),0);
  fillchar(pre,sizeof(pre),0);
  fillchar(vis,sizeof(vis),false);
  queue[1]:=src;
  repeat
    inc(head);
    i:=queue[head];
    for j:=1 to maxpoint do
      if (not vis[j]) and (a[i,j]>0) then
        begin
          inc(tail);
          queue[tail]:=j;
          pre[j]:=i;
          vis[j]:=true;
          if j=dest then exit(true);
        end;
  until head>=tail;
  exit(false);
end;

begin
  readln(t);
  for p:=1 to t do
    begin
      dec(t);
      fillchar(a,sizeof(a),0);
      init;
      ans:=0;
      repeat
        if not bfs(src,dest) then break;
        min:=maxlongint;
        i:=dest;
        while i<>src do
          begin
            if a[pre[i],i]<min then min:=a[pre[i],i];
            i:=pre[i];
          end;
        inc(ans,min);
        i:=dest;
        while i<>src do
          begin
            dec(a[pre[i],i],min);
            inc(a[i,pre[i]],min);
            i:=pre[i];
          end;
      until false;
      writeln('Case #',p,': ',ans);
    end;
end.
