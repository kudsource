const
  maxn=1100;

var
  tcase,m,n,i,k:longint;
  s,st:ansistring;
  sa,tsa,rk,trk,sum,h:array [0..maxn] of longint;

procedure suffix_array(var s:ansistring);
var
  i,j,p:longint;

begin
  fillchar(sum,sizeof(sum),0);
  for i:=1 to n do
    begin
      trk[i]:=ord(s[i]);
      inc(sum[trk[i]]);
    end;
  for i:=1 to 255 do inc(sum[i],sum[i-1]);
  for i:=n downto 1 do
    begin
      sa[sum[trk[i]]]:=i;
      dec(sum[trk[i]]);
    end;
  rk[sa[1]]:=1;
  p:=1;
  for i:=2 to n do
    begin
      if trk[sa[i]]<>trk[sa[i-1]] then inc(p);
      rk[sa[i]]:=p;
    end;
  j:=1;
  while j<n do
    begin
      move(rk,trk,sizeof(rk));
      fillchar(sum,sizeof(sum),0);
      p:=0;
      for i:=n-j+1 to n do
        begin
          inc(p);
          tsa[p]:=i;
        end;
      for i:=1 to n do
        if sa[i]>j then
          begin
            inc(p);
            tsa[p]:=sa[i]-j;
          end;
      for i:=1 to n do
        begin
          rk[i]:=trk[tsa[i]];
          inc(sum[rk[i]]);
        end;
      for i:=1 to n do inc(sum[i],sum[i-1]);
      for i:=n downto 1 do
        begin
          sa[sum[rk[i]]]:=tsa[i];
          dec(sum[rk[i]]);
        end;
      rk[sa[1]]:=1;
      p:=1;
      for i:=2 to n do
        begin
          if (trk[sa[i]]<>trk[sa[i-1]]) or (trk[sa[i]+j]<>trk[sa[i-1]+j]) then inc(p);
          rk[sa[i]]:=p;
        end;
      j:=j shl 1;
    end;
  h[1]:=0;
  p:=0;
  for i:=1 to n do
    begin
      if rk[i]=1 then continue;
      j:=sa[rk[i]-1];
      while s[i+p]=s[j+p] do inc(p);
      h[rk[i]]:=p;
      if p>0 then dec(p);
    end;
end;

function kmp(var p,t:ansistring):longint;
var
  i,j:longint;
  next:array [0..maxn] of longint;

begin
  m:=length(p);
  j:=0;
  next[1]:=0;
  for i:=2 to m do
    begin
      while (j>0) and (p[j+1]<>p[i]) do j:=next[j];
      if p[j+1]=p[i] then inc(j);
      next[i]:=j;
    end;
  j:=0;
  kmp:=0;
  for i:=1 to n do
    begin
      while (j>0) and (p[j+1]<>t[i]) do j:=next[j];
      if p[j+1]=t[i] then inc(j);
      if j=m then
        begin
          inc(kmp);
          j:=next[j];
        end;
    end;
end;

begin
  readln(tcase);
  repeat
    dec(tcase);
    readln(s);
    s:=s+'$';
    n:=length(s);
    suffix_array(s);
    k:=1;
    for i:=2 to n do if h[i]>h[k] then k:=i;
    st:=copy(s,sa[k],h[k]);
    if st='' then writeln('No repetitions found!') else
      writeln(st,' ',kmp(st,s));
  until tcase=0;
end.
