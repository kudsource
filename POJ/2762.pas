{$inline on}
const
  maxn=1001;
  maxm=6001;

var
  edge:array [0..maxm] of record
                            adj,next:longint;
                          end;
  data:array [0..maxm] of record
                            src,dest:longint;
                          end;
  ehead,stack,dfn,low,contract:array [0..maxn] of longint;
  instack:array [0..maxn] of boolean;
  tot,cnt,ans,n,m,t,i,stack_cnt:longint;

procedure insert_edge(p,q:longint); inline;
begin
  if p=q then exit;
  edge[cnt].adj:=q;
  edge[cnt].next:=ehead[p];
  ehead[p]:=cnt;
  inc(cnt);
end;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

procedure push(k:longint); inline;
begin
  inc(stack_cnt);
  stack[stack_cnt]:=k;
end;

function pop:longint; inline; //pop & get top
begin
  pop:=stack[stack_cnt];
  dec(stack_cnt);
end;

procedure dfs(i:longint);
var
  p,q,j:longint;

begin
  p:=ehead[i];
  inc(tot);
  low[i]:=tot;
  dfn[i]:=tot;
  push(i);
  instack[i]:=true;
  while p<>-1 do
    begin
      j:=edge[p].adj;
      if dfn[j]=0 then dfs(j);
      if instack[j] then low[i]:=min(low[i],low[j]);
      p:=edge[p].next;
    end;
  if dfn[i]=low[i] then
    repeat
      j:=pop;
      instack[j]:=false;
      contract[j]:=i;
    until j=i;
end;

procedure init; inline;
begin
  cnt:=0;
  tot:=0;
  fillchar(ehead,sizeof(ehead),255);
  fillchar(dfn,sizeof(dfn),0);
  fillchar(instack,sizeof(instack),false);
  fillchar(contract,sizeof(contract),0);
end;

function toposort(k:longint):longint;
begin
  if ehead[k]=-1 then exit(1) else exit(toposort(edge[ehead[k]].adj)+1);
end;

begin
  readln(t);
  repeat
    dec(t);
    readln(n,m);
    init;
    stack_cnt:=0;
    for i:=1 to m do
      begin
        readln(data[i].src,data[i].dest);
        insert_edge(data[i].src,data[i].dest);
      end;
    for i:=1 to n do if dfn[i]=0 then dfs(i);
    fillchar(ehead,sizeof(ehead),255);
    cnt:=0;
    tot:=0;
    for i:=1 to m do insert_edge(contract[data[i].src],contract[data[i].dest]);
    for i:=1 to n do
      if contract[i]=i then inc(tot);
    ans:=0;
    for i:=1 to n do
      if (contract[i]=i) and (toposort(contract[i])=tot) then inc(ans);
    if ans=1 then writeln('Yes') else writeln('No');
  until t=0;
end.