const
  maxn=10010;

type
  tree=^node;
  node=record
         lson,rson:tree;
       end;

var
  root:tree;
  s:ansistring;
  count:longint;

function addnode(var now:tree):tree;
var
  tmptree:tree;

begin
  if now=nil then
    begin
      new(tmptree);
      now:=tmptree;
      now^.lson:=nil;
      now^.rson:=nil;
      exit(now);
    end;
  exit(addnode(now^.rson));
end;

procedure build(var root:tree);
var
  i,cnt:longint;
  now:tree;
  stack:array [0..maxn] of tree;

begin
  now:=root;
  cnt:=0;
  for i:=1 to length(s) do
    if s[i]='d' then
      begin
        inc(cnt);
        stack[cnt]:=now;
        now:=addnode(now^.lson);
      end
    else
      begin
        now:=stack[cnt];
        dec(cnt);
      end;
end;

function max(p,q:longint):longint;
begin
  if p<q then p:=q;
  exit(p);
end;

function cal_before(root:tree):longint;
begin
  if root=nil then exit(0);
  exit(max(cal_before(root^.lson)+1,cal_before(root^.rson)));
end;

function cal_now(root:tree):longint;
begin
  if root=nil then exit(0);
  exit(max(cal_now(root^.lson),cal_now(root^.rson))+1);
end;

begin
  readln(s);
  count:=0;
  while s<>'#' do
    begin
      inc(count);
      new(root);
      root^.lson:=nil;
      root^.rson:=nil;
      build(root);
      writeln('Tree ',count,': ',cal_before(root)-1,' => ',cal_now(root)-1);
      dispose(root);
      readln(s);
    end;
end.
