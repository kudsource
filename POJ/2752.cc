#include <cstdio>
#include <cstring>
#include <stack>

using std::scanf;
using std::printf;

const int maxn=400000;

int next[maxn];
char s[maxn];

void cal_next(char *s, int n) //s:string, n==strlen(s)
{
	int k=-1;
	memset(next,0,sizeof(next));
	next[0]=-1;
	for (int i=1; i<n; ++i)
	{
		while (k>-1 && s[k+1]!=s[i]) k=next[k];
		if (s[k+1]==s[i]) ++k;
		next[i]=k;
	}
}

int main()
{
	std::stack<int> ans;
	while (scanf("%s",s)!=EOF)
	{
		int n=strlen(s);
		cal_next(s,n);
		ans.push(n);
		for (int i=n-1; next[i]>-1; i=next[i]) ans.push(next[i]+1);
		for (; !ans.empty(); ans.pop()) printf("%d ",ans.top());
		puts("");
	}
	return 0;
}
