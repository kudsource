#include <stdio.h>

#define maxn 1010

int max(int p, int q) 
{
	return p>q?p:q;
}

int main() 
{
	int n,f[maxn][maxn],a[maxn];
	scanf("%d",&n);
	int i,j;
	for (i=1; i<=n; i++) 
	{
		for (j=1; j<=i; j++) scanf("%d",&(a[j]));
		for (j=1; j<=i; j++) f[i][j]=max(f[i-1][j],f[i-1][j-1])+a[j];
	}
	int ans=0;
	for (i=1; i<=n; i++) ans=max(ans,f[n][i]);
	printf("%d\n",ans);
	return 0;
}