#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=100000+10;

char a[maxn],b[maxn];

int n,m;

struct node
{
	int len;
	node *next[26],*pre;
} pool[maxn*10],*root,*last;

int cnt=0;

namespace suffix_automation
{
	void insert(int ch)
	{
		node *p=last,*np=&pool[cnt++];
		np->len=last->len+1;
		for (; p && !p->next[ch]; p=p->pre) p->next[ch]=np;
		last=np;
		if (!p) np->pre=root; else
			if (p->next[ch]->len+1==p->len) np->pre=p; else
			{
				node *q=p->next[ch],*nq=&pool[cnt++];
				*nq=*q;
				nq->len=p->len+1;
				q->pre=np->pre=nq;
				for (; p && p->next[ch]==q; p=p->pre) p->next[ch]=nq;
			}
	}
	
	int solve(char b[])
	{
		node *now=root;
		int res=0,tmp=0;
		for (int i=0; i<m; ++i)
		{
			int ch=b[i]-'a';
			if (now->next[ch]) ++tmp,now=now->next[ch]; else
			{
				while (now && !now->next[ch]) now=now->pre;
				if (!now) tmp=0,now=root; else tmp=now->len+1,now=now->next[ch];
			}
			res=max(res,tmp);
		}
		return res;
	}
}

int main()
{
	scanf("%s%s",a,b);
	root=last=&pool[cnt++];
	n=strlen(a),m=strlen(b);
	for (int i=0; i<n; ++i) suffix_automation::insert(a[i]-'a');
	printf("%d\n",suffix_automation::solve(b));
	return 0;
}

