#include <cstdio>

using namespace std;

int main()
{
	int tcase;
	scanf("%d",&tcase);
	while (tcase--)
	{
		int a,b;
		scanf("%d:%d",&a,&b);
		if (b>0) puts("0"); else
			if (a==12) puts("24"); else
				printf("%d\n",(a+12)%24);
	}
	return 0;
}
