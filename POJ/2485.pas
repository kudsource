const
  maxn=500;

var
  a:array [0..maxn,0..maxn] of longint;
  dis:array [0..maxn] of longint;
  vis:array [0..maxn] of boolean;
  n,i,j,k,p,min:longint;
  ans:int64;

begin
  while not eof do
    begin
      readln(n);
      fillchar(a,sizeof(a),0);
      for i:=1 to n do
        for j:=1 to n do
          read(a[i,j]);
      readln;
      fillchar(vis,sizeof(vis),false);
      fillchar(dis,sizeof(dis),$3F);
      ans:=0;
      dis[1]:=0;
      for i:=1 to n do
        begin
          k:=0;
          for j:=1 to n do
            if (not vis [j]) and ((k=0) or (dis[j]<dis[k])) then k:=j;
          vis[k]:=true;
          inc(ans,dis[k]);
          dis[k]:=0;
          for j:=1 to n do
            if dis[j]>a[k,j] then dis[j]:=a[k,j];
        end;
      writeln(ans);
    end;
end.
