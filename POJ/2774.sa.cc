#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 2e5 + 10;

char s[maxn];
int n, sw, gap;
int sa[maxn], rank[maxn], tmp[maxn], h[maxn];

inline bool cmp(int p, int q) {
	if (rank[p] != rank[q]) return rank[p] < rank[q];
	p += gap, q += gap;
	return (p <= n && q <= n) ? (rank[p] < rank[q]) : (p > q);
}

int main() {
	scanf(" %s", s + 1);
	int sw = strlen(s + 1);
	s[++sw] = '$';
	scanf(" %s", s + sw + 1);
	n = strlen(s + 1);
	s[++n] = '#';
	for (int i = 1; i <= n; ++i) sa[i] = i, rank[i] = s[i];
	for (gap = 1;; gap <<= 1) {
		sort(sa + 1, sa + 1 + n, cmp);
		tmp[1] = 1;
		for (int i = 1; i < n; ++i) tmp[i + 1] = tmp[i] + cmp(sa[i], sa[i + 1]);
		for (int i = 1; i <= n; ++i) rank[sa[i]] = tmp[i];
		if (tmp[n] == n) break;
	}
	int ans = 0, p = 0;
	for (int i = 1, j; i <= n; ++i) {
		if (rank[i] == n) continue;
		j = sa[rank[i] + 1];
		while (s[i + p] == s[j + p]) ++p;
		h[rank[i]] = p;
		if (p) --p;
	}
	for (int i = 1; i < n; ++i)
		if ((sa[i] > sw) ^ (sa[i + 1] > sw)) ans = max(ans, h[i]);
	printf("%d\n", ans);
	return 0;
}
