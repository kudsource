#include <cstdio>
#include <cstring>
#include <map>
#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

const int maxn = 1<<11 + 10, maxm = maxn<<1;

int n,m,f[maxn][2],g[maxn][2];

map<int,int> inv;
vector<int> e[maxn];

int dfn[maxn],low[maxn],scc[maxn],cnt,tot;

stack<int> stk;
bool instk[maxn];

void tarjan(int i) {
	dfn[i] = low[i] = ++tot;
	stk.push(i), instk[i] = true;
	for (vector<int>::iterator p = e[i].begin(); p != e[i].end(); ++p) {
		int j = *p;
		if (!dfn[j]) tarjan(j);
		if (instk[j]) low[i] = min(low[i],low[j]);
	}
	if (dfn[i] == low[i]) {
		int j;
		++cnt;
		do {
			j = stk.top(), stk.pop(), instk[j] = false;
			scc[j] = cnt;
		} while (j != i);
	}
}

bool init() {
	if (scanf("%d%d",&n,&m) == EOF) return false;
	if (n == 0 && m == 0) return false;
	int p,q;
	inv.clear();
	for (int i=1; i<=n; ++i) {
		scanf("%d%d",&p,&q);
		inv[p] = q, inv[q] = p;
	}
	for (int i=1; i<=m; ++i) scanf("%d%d",&g[i][0],&g[i][1]);
	n<<=1;
	return true;
}

void build(int m) {
	for (int i=0; i<n; ++i) e[i].clear();
	for (int i=1; i<=m; ++i) {
		int p = g[i][0], q = g[i][1];
		e[inv[p]].push_back(q);
		e[inv[q]].push_back(p);
	}
}

bool solve(int x) {
	build(x);
	memset(dfn,0,sizeof dfn);
	cnt = tot = 0;
	for (int i=0; i<n; ++i) if (!dfn[i]) tarjan(i);
	for (int i=0; i<n; ++i) if (scc[i] == scc[inv[i]]) return false;
	return true;
}

int main() {
	while (init()) {
		int l = 0, r = m+1;
		while (l+1 < r) {
			int mid = (l+r) >> 1;
			if (solve(mid)) l = mid; else r = mid;
		}
		printf("%d\n",l);
	}
	return 0;
}
