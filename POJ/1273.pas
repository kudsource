const
  maxn=200;
  oo=1 shl 30;

var
  n,m,i,src,dest,cost,ans,min:longint;
  a:array [0..maxn,0..maxn] of longint;
  pre:array [0..maxn] of longint;

function bfs(src,dest:longint):boolean;
var
  head,tail,i,j:longint;
  queue:array [0..maxn shl 1] of longint;
  vis:array [0..maxn] of boolean;

begin
  fillchar(queue,sizeof(queue),0);
  fillchar(pre,sizeof(pre),0);
  fillchar(vis,sizeof(vis),false);
  head:=0;
  tail:=1;
  queue[1]:=src;
  repeat
    inc(head);
    i:=queue[head];
    for j:=1 to n do
      if (not vis[j]) and (a[i,j]>0) then
        begin
          inc(tail);
          queue[tail]:=j;
          pre[j]:=i;
          vis[j]:=true;
          if queue[tail]=dest then exit(true);
        end;
  until head>=tail;
  exit(false);
end;

begin
  while not eof do
    begin
      readln(m,n);
      fillchar(a,sizeof(a),0);
      for i:=1 to m do
        begin
          readln(src,dest,cost);
          inc(a[src,dest],cost);
        end;
      ans:=0;
      repeat
        if not bfs(1,n) then break;
        i:=n;
        min:=oo;
        while i<>1 do
          begin
            if a[pre[i],i]<min then min:=a[pre[i],i];
            i:=pre[i];
          end;
        inc(ans,min);
        i:=n;
        while i<>1 do
          begin
            dec(a[pre[i],i],min);
            inc(a[i,pre[i]],min);
            i:=pre[i];
          end;
      until false;
      writeln(ans);
    end;
end.
