const
  maxn=1000+10;
  maxm=100000+10;

var
  e:array [0..maxm] of record
                         adj,next,cap,cost:longint;
                       end;
  h,pre,pos,dis:array [0..maxn] of longint;
  cnt,n,m:longint;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

procedure link(p,q,cap,cost:longint);
begin
  e[cnt].adj:=q;
  e[cnt].next:=h[p];
  e[cnt].cap:=cap;
  e[cnt].cost:=cost;
  h[p]:=cnt;
  inc(cnt);
  e[cnt].adj:=p;
  e[cnt].next:=h[q];
  e[cnt].cap:=0;
  e[cnt].cost:=-cost;
  h[q]:=cnt;
  inc(cnt);
end;

procedure init;
var
  i,p,q,c:longint;

begin
  readln(n,m);
  fillchar(h,sizeof(h),255);
  for i:=1 to m do
    begin
      readln(p,q,c);
      link(p,q,1,c);
      link(q,p,1,c);
    end;
  link(0,1,2,0);
  link(n,n+1,2,0);
end;

function spfa(s,t:longint):boolean;
var
  i,p:longint;
  q:array [0..maxn*10] of longint;
  vis:array [0..maxn] of boolean;
  qf,qr:^longint;

begin
  fillchar(pre,sizeof(pre),255);
  fillchar(vis,sizeof(vis),false);
  qf:=@q[0];
  qr:=qf;
  fillchar(dis,sizeof(dis),127);
  pre[s]:=s;
  dis[s]:=0;
  vis[s]:=true;
  inc(qr);
  qr^:=s;
  repeat
    inc(qf);
    p:=h[qf^];
    vis[qf^]:=false;
    while p<>-1 do
      begin
        i:=e[p].adj;
        if (e[p].cap>0) and (dis[qf^]+e[p].cost<dis[i]) then
          begin
            dis[i]:=dis[qf^]+e[p].cost;
            pre[i]:=qf^;
            pos[i]:=p;
            if not vis[i] then
              begin
                inc(qr);
                qr^:=i;
                vis[i]:=true;
              end;
          end;
        p:=e[p].next;
      end;
  until qf>=qr;
  exit(pre[t]<>-1);
end;

function flow(s,t:longint):longint;
var
  tmp,i:longint;

begin
  flow:=0;
  while spfa(s,t) do
    begin
      tmp:=maxlongint;
      i:=t;
      while i<>s do
        begin
          tmp:=min(tmp,e[pos[i]].cap);
          i:=pre[i];
        end;
      inc(flow,dis[t]*tmp);
      i:=t;
      while i<>s do
        begin
          dec(e[pos[i]].cap,tmp);
          inc(e[pos[i] xor 1].cap,tmp);
          i:=pre[i];
        end;
    end;
end;

begin
  init;
  writeln(flow(0,n+1));
end.
