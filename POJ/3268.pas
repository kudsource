{$inline on}

const
  maxn=1010;
  maxm=100010;

type
  heap_type=record
              adj,cost:longint;
            end;

var
  edge:array [0..maxm] of record
                            adj,next,cost:longint;
                          end;
  ehead,dis,a:array [0..maxn] of longint;
  data:array [0..maxm] of record
                            src,dest,cost:longint;
                          end;
  heap:array [0..maxm*2] of heap_type;
  cnt,heap_cnt,n,m,i,src,ans:longint;

procedure insert_edge(p,q,c:longint); inline;
begin
  if p=q then exit;
  edge[cnt].adj:=q;
  edge[cnt].next:=ehead[p];
  edge[cnt].cost:=c;
  ehead[p]:=cnt;
  inc(cnt);
end;

operator < (p,q:heap_type) res:boolean;
begin
  res:=p.cost<q.cost;
end;

operator > (p,q:heap_type) res:boolean;
begin
  res:=p.cost>q.cost;
end;

procedure swap(var p,q:heap_type);
var
  t:heap_type;

begin
  t:=p;
  p:=q;
  q:=t;
end;

procedure heap_up(k:longint);
begin
  if k=1 then exit;
  if heap[k shr 1]>heap[k] then
    begin
      swap(heap[k],heap[k shr 1]);
      heap_up(k shr 1);
    end;
end;

procedure heap_down(k:longint);
var
  p:longint;

begin
  p:=k shl 1;
  if p>heap_cnt then exit;
  if (p+1<=heap_cnt) and (heap[p+1]<heap[p]) then inc(p);
  if heap[p]<heap[k] then
    begin
      swap(heap[k],heap[p]);
      heap_down(p);
    end;
end;

procedure heap_insert(k,c:longint);
begin
  inc(heap_cnt);
  heap[heap_cnt].adj:=k;
  heap[heap_cnt].cost:=c;
  heap_up(heap_cnt);
end;

function heap_deletemin:heap_type;
begin
  heap_deletemin:=heap[1];
  heap[1]:=heap[heap_cnt];
  dec(heap_cnt);
  heap_down(1);
end;

procedure dijsktra(src:longint);
var
  vis:array [0..maxn] of boolean;
  k:heap_type;
  i,j,w,p:longint;

begin
  fillchar(vis,sizeof(vis),false);
  fillchar(dis,sizeof(dis),$3F);
  dis[src]:=0;
  heap_insert(src,0);
  while heap_cnt>0 do 
    begin
      k:=heap_deletemin;
      vis[k.adj]:=true;
      p:=ehead[k.adj];
      while p<>-1 do
        begin
          j:=edge[p].adj;
          w:=edge[p].cost;
          if (not vis[j]) and (dis[k.adj]+w<dis[j]) then
            begin
              dis[j]:=dis[k.adj]+w;
              heap_insert(j,dis[j]);
            end;
          p:=edge[p].next;
        end;
    end;
end;

procedure init; inline;
begin
  fillchar(ehead,sizeof(ehead),255);
  fillchar(edge,sizeof(edge),0);
  fillchar(heap,sizeof(heap),0);
  cnt:=0;
  heap_cnt:=0;
end;

function max(p,q:longint):longint; inline;
begin
  if p>q then exit(p) else exit(q);
end;

begin
{$IFNDEF ONLINE_JUDGE}
  assign(input,'sparty.in');
  assign(output,'sparty.out');
  reset(input);
  rewrite(output);
{$ENDIF}
  readln(n,m,src);
  init;
  for i:=1 to m do
    begin
      readln(data[i].src,data[i].dest,data[i].cost);
      insert_edge(data[i].src,data[i].dest,data[i].cost);
    end;
  dijsktra(src);
  a:=dis;
  init;
  for i:=1 to m do insert_edge(data[i].dest,data[i].src,data[i].cost);
  dijsktra(src);
  ans:=0;
  for i:=1 to n do ans:=max(ans,dis[i]+a[i]);
  writeln(ans);
{$IFNDEF ONLINE_JUDGE}
  close(input);
  close(output);
{$ENDIF}
end.
