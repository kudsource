const
  maxn=102;

var
  n,m,head,tail,src,dest:longint;
  a:array [0..maxn,0..maxn] of longint; //adjacency matrix
  height,e:array [0..maxn] of longint;  //height & water
  queue:array [0..maxint] of longint;

procedure getint(var k:longint);
var
  ch:char;

begin
  k:=0;
  read(ch);
  while not (ch in ['0'..'9']) do read(ch);
  repeat
    k:=k*10+ord(ch)-48;
    read(ch);
  until not (ch in ['0'..'9']);
end;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

procedure preflow(src:longint); inline; //it's a one-time op
var
  j:longint;

begin
  for j:=0 to n+1 do
    if (a[src,j]>0) and (j<>src) then
      begin
        e[j]:=a[src,j];
        inc(a[j,src],e[j]);
        if (j<>src) and (j<>dest) then
          begin
            inc(tail);
            queue[tail]:=j;
          end;
      end;
end;

procedure push(i,j:longint); inline;
var
  k:longint;

begin
  k:=min(e[i],a[i,j]); //minimum of point i's water & capacity of edge i->j
  dec(e[i],k);
  inc(e[j],k);
  dec(a[i,j],k); //like the ops in Ford-Fulkerson Algorithm
  inc(a[j,i],k);
end;

procedure relabel(i:longint); //after: point i's label(height) is equal to
var                           //       the lowest point connected with i
  j:longint;

begin
  for j:=0 to n+1 do
    if a[i,j]>0 then height[i]:=min(height[i],height[j]);
  inc(height[i]);
end;

function flow(src,dest:longint):longint;
var
  i,j:longint;

begin
  fillchar(height,sizeof(height),0);
  fillchar(e,sizeof(e),0);
  head:=0;
  tail:=1;
  height[src]:=n+1;
  preflow(src);
  repeat
    inc(head);
    i:=queue[head];
    relabel(i);
    for j:=0 to n+1 do
      if (height[i]=height[j]+1) and (a[i,j]>0) then
        begin
          if (e[j]=0) and (j<>src) and (j<>dest) then
            begin
              inc(tail);
              queue[tail]:=j;
            end;
          push(i,j);
          if e[i]=0 then break;
        end;
    if e[i]>0 then
      begin
        inc(tail);
        queue[tail]:=i;
      end;
  until head>=tail;
  exit(e[dest]);
end;

procedure init;
var
  np,nc,i,s,t:longint;

begin
  read(n,np,nc,m);
  src:=n;
  dest:=n+1;
  for i:=1 to m do
    begin
      getint(s);
      getint(t);
      getint(a[s,t]);
    end;
  for i:=1 to np do
    begin
      getint(t);
      getint(a[src,t]);
    end;
  for i:=1 to nc do
    begin
      getint(s);
      getint(a[s,dest]);
    end;
  while eoln xor eof do readln; //skip the damn whitespaces (avoid WA)
end;

begin
  while not eof do
    begin
      fillchar(a,sizeof(a),0);
      init;
      writeln(flow(src,dest));
    end;
end.
