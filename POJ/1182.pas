var
  f,g:array [0..50000] of longint;
  n,k,d,x,y,i,ans:longint;

function find(p:longint):longint;
begin
  if f[p]=p then exit(p);
  find:=find(f[p]);
  g[p]:=(g[f[p]]+g[p]) mod 3;
  f[p]:=find;
end;

procedure merge(p,q,r:longint);
var
  i,j,t:longint;

begin
  if (p>n) or (q>n) then begin inc(ans); exit; end;
  i:=find(p); j:=find(q);
  if i=j then
    begin
      t:=(g[p]-g[q]+3) mod 3;
      if t<>r then inc(ans);
      exit;
    end;
  f[i]:=j;
  g[i]:=(g[q]-g[p]+r+3) mod 3;
end;

begin
  readln(n,k);
  for i:=1 to n do f[i]:=i;
  for i:=1 to k do
    begin
      readln(d,x,y);
      merge(x,y,d-1);
    end;
  writeln(ans);
end.