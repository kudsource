const
	maxn=5010;
var
	n,i,j:longint;
	a,b:ansistring;
	f:array [0..maxn,0..maxn] of integer;
	
function max(p,q:integer):integer;
begin
	if p>q then exit(p) else exit(q);
end;
	
begin
	readln(n);
	readln(a);
	for i:=length(a) downto 1 do b:=b+a[i];
	for i:=1 to n do 
		for j:=1 to n do 
			begin
				f[i,j]:=max(f[i,j-1],f[i-1,j]);
				if a[i]=b[j] then f[i,j]:=max(f[i,j],f[i-1,j-1]+1);
			end;
	writeln(n-f[n,n]);
end.