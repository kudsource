#include <cstdio>
#include <cctype>
#include <cstring>

using namespace std;

const int maxn=100010;

int n,edge_tot,tot,ehead[maxn],st[maxn],ed[maxn],tree[maxn],flag[maxn];

struct 
{
	int adj,next;
} edge[maxn<<1];

void insedge(int x, int y)
{
	edge[edge_tot].adj=y;
	edge[edge_tot].next=ehead[x];
	ehead[x]=edge_tot++;
}

void dfs(int root)
{
	flag[root]=1;
	st[root]=++tot;
	int now;
	for (now=ehead[root]; now; now=edge[now].next)
		if (!flag[edge[now].adj]) dfs(edge[now].adj);
	ed[root]=tot;
}

inline int lowbit(int x) 
{ 
	return x & ( x ^ ( x - 1 ) );
}

inline void update(int k, int tmp)
{
	for (int j=k; j<=n; j+=lowbit(j)) 
		tree[j]+=tmp;
}

inline int query(int k)
{
	int sum=0;
	while (k)
	{
		sum+=tree[k];
		k-=lowbit(k);
	}
	return sum;
}

int main()
{
	scanf("%d",&n);
	edge_tot=1;
	for (int i=1; i<n; i++)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		insedge(x,y);
		insedge(y,x);
	}
	tot=0;
	dfs(1);
	for (int i=1; i<=n; i++) 
		update(st[i],1);
	int m;
	scanf("%d",&m);
	char order;
	int k;
	while (m--)
	{
		getchar();
		scanf("%c%d",&order,&k);
		switch (order)
		{
			case 'C':{ flag[k]=-flag[k]; update(st[k],flag[k]); break; }
			case 'Q':{ printf("%d\n",query(ed[k])-query(st[k]-1)); break; }
		}
	}
	return 0;
}
