#include <cstdio>
#include <algorithm>

//#define HDU
//for submit to hdu2665

const int maxn=100000+10;

struct node
{
	int l,r,sum;
	node *lson,*rson;
	inline int mid() { return (l+r)>>1; }
} pool[maxn*20],*root[maxn];

int cnt,data[maxn],sorted[maxn],hash[maxn];

namespace SegmentTree
{
	node* build(int l, int r)
	{
		node *p=&pool[cnt++];
		p->l=l,p->r=r,p->sum=0;
		if (l<r)
		{
			p->lson=build(l,p->mid());
			p->rson=build(p->mid()+1,r);
		}
		return p;
	}
	node* modify(node *orig, int num)
	{
		node *p=&pool[cnt++];
		*p=*orig;
		p->sum++;
		if (orig->l < orig->r)
		{
			if (num <= orig->mid()) p->lson=modify(orig->lson,num); else p->rson=modify(orig->rson,num);
		}
		return p;
	}
	int query(node *rt, node *lt, int k)
	{
		if (lt->l == lt->r) return lt->l;
		int tmp=rt->lson->sum - lt->lson->sum;
		if (k<=tmp) return query(rt->lson,lt->lson,k); else return query(rt->rson,lt->rson,k-tmp);
	}
}

int main()
{
	int tcase;
#ifdef HDU
	std::scanf("%d",&tcase);
#else
	tcase=1;
#endif
	while (tcase--)
	{
		int n,m;
		std::scanf("%d%d",&n,&m);
		for (int i=1; i<=n; ++i) std::scanf("%d",&data[i]),sorted[i]=data[i];
		std::sort(sorted+1,sorted+n+1);
		cnt=0;
		int tot=0;
		for (int i=1; i<=n; ++i) hash[++tot]=sorted[i];
		root[0]=SegmentTree::build(1,tot);
		for (int i=1; i<=n; ++i)
		{
			int tmp=std::lower_bound(sorted+1,sorted+n+1,data[i])-sorted;
			root[i]=SegmentTree::modify(root[i-1],tmp);
		}
		int l,r,k;
		while (m--)
		{
			std::scanf("%d%d%d",&l,&r,&k);
			std::printf("%d\n",hash[SegmentTree::query(root[r],root[l-1],k)]);
		}
	}
}
