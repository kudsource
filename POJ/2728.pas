{$inline on}

const
  e=1e-4;
  maxn=1010;
  oo=1e10;

var
  a:array [0..maxn] of record
                         x,y,z:longint;
                       end;
  cost,len:array [0..maxn,0..maxn] of double;
  l,r,m:double;
  n,i,j:longint;

function cal_cost(p,q:longint):double; inline;
begin
  exit(abs(a[p].z-a[q].z));
end;

function cal_dis(p,q:longint):double; inline;
begin
  exit(sqrt(sqr(a[p].x-a[q].x)+sqr(a[p].y-a[q].y)));
end;

function cal(p,q:longint; rate:double):double; inline;
begin
  exit(cost[p,q]-len[p,q]*rate);
end;

function prim(var x:double):double;
var
  vis:array [0..maxn] of boolean;
  dis:array [0..maxn] of double;
  pre:array [0..maxn] of longint;
  i,j,k:longint;
  total_len,total_cost:double;

begin
  for i:=2 to n do dis[i]:=oo;
  dis[0]:=0;
  fillchar(vis,sizeof(vis),false);
  fillchar(pre,sizeof(pre),0);
  prim:=0;
  total_len:=0;
  total_cost:=0;
  for i:=1 to n do
    begin
      k:=0;
      for j:=1 to n do
        if (not vis[j]) and ((k=0) or (dis[j]<dis[k])) then k:=j;
      total_len:=total_len+len[pre[k],k];
      total_cost:=total_cost+cost[pre[k],k];
      vis[k]:=true;
      for j:=1 to n do
        if (not vis[j]) and (dis[j]>cal(k,j,x)) then
          begin
            dis[j]:=cal(k,j,x);
            pre[j]:=k;
          end;
    end;
  exit(total_cost/total_len);
end;

begin
  readln(n);
  while n>0 do
    begin
      for i:=1 to n do
        begin
          readln(a[i].x,a[i].y,a[i].z);
          for j:=1 to i-1 do
            begin
              cost[i,j]:=cal_cost(i,j);
              len[i,j]:=cal_dis(i,j);
              cost[j,i]:=cost[i,j];
              len[j,i]:=len[i,j];
            end;
        end;
      l:=0;
      repeat
        r:=prim(l);
        if abs(r-l)<e then break;
        l:=r;
      until false;
      writeln(r:0:3);
      readln(n);
    end;
end.
