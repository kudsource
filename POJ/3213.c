#include <stdio.h>
#include <stdbool.h>

#define maxn 1010
#define rep(i,n) for(i=0; i<n; ++i)

int n,p,m,a[maxn][maxn],b[maxn][maxn];

int mul(int i, int j) {
	int k,res=0;
	rep(k,p) res+=(a[i][k]*b[k][j]);
	return res;
}

int main() {
	scanf("%d%d%d",&n,&p,&m);
	int i,j;
	rep(i,n)
		rep(j,p)
			scanf("%d",&a[i][j]);
	rep(i,p)
		rep(j,m)
			scanf("%d",&b[i][j]);
	bool flag=false;
	int p,q,k,ans=0;
	rep(i,n) {
		rep(j,m) {
			scanf("%d",&k);
			if (!flag && (ans=mul(i,j)) != k) {
				flag=true;
				p=i,q=j;
			}
		}
	}
	if (flag) {
		puts("No");
		printf("%d %d\n%d\n",++p,++q,ans);
	} else puts("Yes");
	return 0;
}
