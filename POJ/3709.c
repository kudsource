/*
dp[i] = min{dp[j] + sum[i] - sum[j] - (i - j) * data[j+1]}

dp[k] - sum[k] - (i - k) * data[k+1] < dp[j] - sum[j] - (i - j) * data[j+1]

y(k) = dp[k] - sum[k] + k * data[k+1]
x(k) = data[k+1]

y(k) - y(j) < i * (x(k) - x(j))

j < k ( x(j) < x(k) )

slope < i

j > k ( x(j) > x(k) )

slope > i
*/
#include <stdio.h>
#include <limits.h>
#include <string.h>

#define maxn 500010

typedef long long int64;

int tcase,n,k,deque[maxn];
int64 dp[maxn],data[maxn],sum[maxn];

inline int64 dy(int p, int q) { return (dp[p] - sum[p] + p * data[p+1]) - (dp[q] - sum[q] + q * data[q+1]); }
inline int64 dx(int p, int q) { return data[p+1] - data[q+1]; }

inline int64 cal(int j, int i) {
	return (i-j >= k) ? (dp[j] + sum[i] - sum[j] - (i - j) * data[j+1]) : INT_MAX;
}

int main() {
	scanf("%d",&tcase);
	while (tcase--) {
		scanf("%d%d",&n,&k);
		sum[0] = dp[0] = 0;
		int i,*qf,*qr;
		for (i = 1; i <= n; ++i) {
			scanf("%lld",data+i);
			sum[i] = sum[i-1] + data[i];
		}
		qf = qr = &deque[0];
		for (i = 1; i <= n; ++i) {
			if (i > k) {
				while (qf < qr && dy(*(qr-1),*qr) * dx(*qr,i-k) >= dy(*qr,i-k) * dx(*(qr-1),*qr)) --qr; //avoid double
				*++qr = i-k;
			}
			while (qf < qr && dy(*(qf+1),*qf) <= i * dx(*(qf+1),*qf)) ++qf;
			dp[i] = cal(*qf,i);
		}
		printf("%lld\n",dp[n]);
	}
	return 0;
}
