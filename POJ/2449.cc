
#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>
#include <utility>

using namespace std;

const int maxn = 1e3 + 10;

int n,m,s,t,k,dis[maxn];

vector< pair<int,int> > epool[maxn],repool[maxn];

struct state {
	int v,g,h;
};

bool operator< (const state &p, const state &q) {
	return p.g + p.h > q.g + q.h;
}

void sssp(int s) {
	memset(dis,10,sizeof dis);
	static queue<int> q;
	static bool inq[maxn] = {false};
	q.push(s);
	dis[s] = 0;
	inq[s] = true;
	vector< pair<int,int> >::iterator iter;
	while (!q.empty()) {
		int i = q.front();
		q.pop();
		inq[i] = false;
		for (iter = repool[i].begin(); iter != repool[i].end(); ++iter) {
			int j = iter->first, c = iter->second;
			if (dis[i] + c < dis[j]) {
				dis[j] = dis[i] + c;
				if (!inq[j]) {
					inq[j] = true;
					q.push(j);
				}
			}
		}
	}
}

int kth(int s, int t, int k) {
	state cur,nxt;
	static priority_queue<state> q;
	static int cnt[maxn] = {0};
	cur.v = s;
	cur.g = 0;
	cur.h = dis[s];
	q.push(cur);
	vector< pair<int,int> >::iterator iter;
	while (!q.empty()) {
		cur = q.top(), q.pop();
		int i = cur.v;
		++cnt[i];
		if (cnt[i] > k) continue;
		if (cnt[t] == k) return cur.g;
		for (iter = epool[i].begin(); iter != epool[i].end(); ++iter) {
			int j = iter->first, c = iter->second;
			nxt.v = j;
			nxt.g = cur.g + c;
			nxt.h = dis[j];
			q.push(nxt);
		}
	}
	return -1;
}

int main() {
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; ++i) {
		int p,q,c;
		scanf("%d%d%d",&p,&q,&c);
		epool[p].push_back(make_pair(q,c));
		repool[q].push_back(make_pair(p,c));
	}
	scanf("%d%d%d",&s,&t,&k);
	if (s == t) ++k;
	sssp(t);
	printf("%d\n",kth(s,t,k));
	return 0;
}
