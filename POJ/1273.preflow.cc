#include <cstdio>
#include <cstring>
#include <list>
#include <algorithm>

#define in(x) scanf("%d",&x)
#define out(x) printf("%d\n",x)

using namespace std;

const int maxn = 200 + 10, maxm = maxn*maxn, inf = 1e9;

int n, m;

int to[maxm], next[maxm], cap[maxm], etot;
int adj[maxn];

void link(int a, int b, int c) {
	to[etot] = b;
	next[etot] = adj[a];
	cap[etot] = c;
	adj[a] = etot++;
	to[etot] = a;
	next[etot] = adj[b];
	cap[etot] = 0;
	adj[b] = etot++;
}

int excess[maxn], h[maxn];

void push(int a, int i) {
	int k = min(cap[i], excess[a]), b = to[i];
	excess[a] -= k;
	excess[b] += k;
	cap[i] -= k;
	cap[i^1] += k;
}

void preflow(int a) {
	excess[a] = inf;
	for (int i = adj[a]; i != -1; i = next[i])
		push(a, i);
}

void relabel(int a) {
	for (int i = adj[a]; i != -1; i = next[i])
		if (cap[i]) h[a] = min(h[a], h[to[i]]);
	++h[a];
}

void discharge(int a) {
	while (excess[a]) {
		for (int i = adj[a]; i != -1; i = next[i]) {
			int b = to[i];
			if (h[a] == h[b] + 1) push(a, i);
			if (!excess[a]) return;
		}
		relabel(a);
	}
}

int flow(int s, int t) {
	memset(h, 0, sizeof h);
	memset(excess, 0, sizeof excess);
	h[s] = n;
	preflow(s);
	static list<int> q;
	q.clear();
	for (int i = 1; i <= n; ++i)
		if (i != s && i != t) q.push_back(i);
	for (list<int>::iterator iter = q.begin(); iter != q.end(); ++iter) {
		int cur = *iter;
		int old_h = h[cur];
		discharge(cur);
		if (h[cur] > old_h) {
			q.erase(iter);
			q.push_front(cur);
			iter = q.begin();
		}
	}
	return excess[t];
}

int main() {
	while (in(m) != EOF) {
		in(n);
		memset(adj, -1, sizeof adj);
		etot = 0;
		for (int i = 1, a, b, c; i <= m; ++i) {
			in(a), in(b), in(c);
			link(a, b, c);
		}
		out(flow(1, n));
	}
	return 0;
}
