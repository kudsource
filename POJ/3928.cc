#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=100010;

int data[maxn],tree[maxn];
int smaller_before[maxn],smaller_after[maxn],larger_before[maxn],larger_after[maxn];

inline int lowbit(int x)
{
	return x & (x ^ (x-1));
}

inline void add(int k)
{
	for (; k<=maxn; k+=lowbit(k)) tree[k]++;
}

inline int sum(int k)
{
	int result=0;
	for (; k>0; k-=lowbit(k)) result+=tree[k];
	return result;
}

int main()
{
	int t;
	scanf("%d",&t);
	while (t--)
	{
		int n;
		scanf("%d",&n);
		memset(tree,0,sizeof(tree));
		for (int i=1; i<=n; i++) 
		{
			scanf("%d",&data[i]);
			add(data[i]);
			smaller_before[i]=sum(data[i]-1);
			larger_before[i]=sum(maxn)-sum(data[i]);
		}
		memset(tree,0,sizeof(tree));
		long long ans=0;
		for (int i=n; i>0; i--)
		{
			add(data[i]);
			smaller_after[i]=sum(data[i]-1);
			larger_after[i]=sum(maxn)-sum(data[i]);
			ans+=(smaller_after[i]*larger_before[i]);
			ans+=(smaller_before[i]*larger_after[i]);
		}
		printf("%lld\n",ans);
	}
	return 0;
}
