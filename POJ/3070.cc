#include <cstdio>
#include <cstring>

using namespace std;

const int mod=10000;

typedef int matrix[2][2];

matrix e={0,1,1,1},res;

void mul(matrix& res, const matrix& ano)
{
	matrix tmp={};
	for (int i=0; i<2; ++i)
		for (int j=0; j<2; ++j)
			for (int k=0; k<2; ++k)
			{
				tmp[i][j]+=(res[i][k]*ano[k][j])%mod;
				tmp[i][j]%=mod;
			}
	memcpy(res,tmp,sizeof(matrix));
}

void solve(int k, matrix& res)
{
	if (k==1)
	{
		memcpy(res,e,sizeof(matrix));
		return;
	}
	solve(k/2,res);
	mul(res,res);
	if (k&1) mul(res,e);
}

int main()
{
	int n;
	do
	{
		scanf("%d",&n);
		if (n==-1) break;
		if (!n) { puts("0"); continue; }
		solve(--n,res);
		printf("%d\n",res[1][1]);
	} while (1);
	return 0;
}
