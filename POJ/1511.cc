/*
Algorithm: Dijkstra + Heap (STL priority_queue<int>)
Time: 1922MS(C++), 2204MS(G++)
even faster than SPFA, which used 2094MS(Pascal)
*/
#pragma GCC optimize ("O2")

#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>

using namespace std;

const int maxn=1000001,maxm=maxn;

int ehead[maxn],cnt=0,n,m;
long long dis[maxn];
bool vis[maxn];

struct
{
	int adj,next,cost;
} edge[maxm];

struct
{
	int src,dest,cost;
} data[maxm];

struct node
{
	int v,cost;
	node(int p, int q)
	{
		v=p,cost=q;
	}
};

inline void insert_edge(int p, int q, int c)
{
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	edge[cnt].cost=c;
	ehead[p]=cnt++;
}

inline node make_node(int p, int q)
{
	node tmp(p,q);
	return tmp;
}

class cmp
{
	public:
		inline bool operator() (const node &x, const node &y)
		{
			return x.cost>y.cost;
		}
};

inline void init()
{
	memset(ehead,255,sizeof(ehead));
	memset(edge,0,sizeof(edge));
}


long long dijkstra()
{
	priority_queue<node, vector<node>, cmp> heap;
	memset(dis,0x7F,sizeof(dis));
	memset(vis,false,sizeof(vis));
	heap.push(make_node(1,0));
	dis[1]=0;
	while (!heap.empty())
	{
		node k=heap.top();
		heap.pop();
		int i=k.v;
		vis[i]=true;
		for (int p=ehead[i]; p!=-1; p=edge[p].next)
		{
			int j=edge[p].adj,w=edge[p].cost;
			if (!vis[j] && dis[i]+w<dis[j])
			{
				dis[j]=dis[i]+w;
				heap.push(make_node(j,dis[j]));
			}
		}
	}
	long long tmp=0;
	for (int i=1; i<=n; ++i) tmp+=dis[i];
	return tmp;
}

int main()
{
	int t;
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d",&n,&m);
		init();
		for (int i=1; i<=m; ++i)
		{
			scanf("%d%d%d",&data[i].src,&data[i].dest,&data[i].cost);
			insert_edge(data[i].src,data[i].dest,data[i].cost);
		}
		long long ans=dijkstra();
		init();
		for (int i=1; i<=m; ++i) insert_edge(data[i].dest,data[i].src,data[i].cost);
		ans+=dijkstra();
		printf("%lld\n",ans);
	}
	return 0;
}
