#include <cstdio>
#include <cstring>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

//1 : choose, 2 : not choose
//p : 0, p+n : 1

const int maxn = 2010;

int n,m;
vector<int> epool[maxn];

void AND(int p, int q, int val) {
	if (val) { // p && q == 1
		epool[p].push_back(p+n);
		epool[q].push_back(q+n);
	} else { // p && q == 0
		epool[p+n].push_back(q);
		epool[q+n].push_back(p);
	}
}

void OR(int p, int q, int val) {
	if (!val) { // p || q == 0
		epool[p+n].push_back(p);
		epool[q+n].push_back(q);
	}
	else { // p || q == 1
		epool[p].push_back(q+n);
		epool[q].push_back(p+n);
	}
}

void XOR(int p, int q, int val) {
	if (val) { // p ^ q == 1
		epool[p].push_back(q+n);
		epool[q].push_back(p+n);
		epool[p+n].push_back(q);
		epool[q+n].push_back(p);
	} else { // p ^ q == 0
		epool[p].push_back(q);
		epool[q].push_back(p);
		epool[p+n].push_back(q+n);
		epool[q+n].push_back(p+n);
	}
}

stack<int> stk;
bool instack[maxn];

int scc[maxn],dfn[maxn],low[maxn],tot,cnt;

void tarjan(int i) {
	dfn[i] = low[i] = ++tot;
	instack[i] = true;
	stk.push(i);
	vector<int>::iterator iter;
	for (iter = epool[i].begin(); iter != epool[i].end(); ++iter) {
		int j = *iter;
		if (!dfn[j]) tarjan(j);
		if (instack[j]) low[i] = min(low[i],low[j]);
	}
	if (dfn[i] == low[i]) {
		int j;
		++cnt;
		do {
			j = stk.top(), stk.pop();
			instack[j] = false;
			scc[j] = cnt;
		} while (j != i);
	}
}

int main() {
	scanf("%d%d",&n,&m);
	int p,q,val;
	char op[10];
	while (m--) {
		scanf(" %d %d %d %s",&p,&q,&val,op);
		switch (op[0]) {
			case 'A':
				AND(p,q,val);
				break;
			case 'O':
				OR(p,q,val);
				break;
			case 'X':
				XOR(p,q,val);
				break;
		}
	}
	for (int i=0; i<n; ++i) {
		if (!scc[i]) tarjan(i);
		if (scc[i] == scc[i+n]) return puts("NO"), 0;
	}
	puts("YES");
	return 0;
}
