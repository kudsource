var
  data,father,son:array [0..100] of longint;
  flag:array [0..100] of boolean;
  n,i,p:longint;

procedure dfs(root:longint);
var
  i:longint;

begin
  if root=0 then exit;
  write('(',root);
  for i:=1 to n do
    if father[i]=root then
      begin
        write(' ');
        dfs(i);
      end;
  write(')');
end;

begin
  while not eof do
    begin
      n:=1;
      while not eoln do
        begin
          read(data[n]);
          inc(son[data[n]]);
          inc(n);
        end;
      readln;
      fillchar(father,sizeof(father),0);
      fillchar(flag,sizeof(flag),false);
      p:=0;
      for i:=1 to n-1 do
        begin
          for p:=1 to n do
            if (father[p]=0) and (son[p]=0) then break;
          dec(son[data[i]]);
          father[p]:=data[i];
        end;
      dfs(n);
      writeln;
    end;
end.
