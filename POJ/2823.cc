#include <iostream>
#include <deque>
#include <vector>

using namespace std;

const int maxn=1000000+1;

deque<int> queue[2];
vector<int> ans[2];

int a[maxn];

int main()
{
	ios::sync_with_stdio(false);
	int n,k;
	cin>>n>>k;
	int i;
	for (i=1; i<=n; ++i) cin>>a[i];
	for (i=1; i<=n; ++i)
	{
		while (!queue[0].empty() && a[i]>a[queue[0].back()]) queue[0].pop_back();
		while (!queue[1].empty() && a[i]<a[queue[1].back()]) queue[1].pop_back();
		while (!queue[0].empty() && queue[0].front()<i-k+1) queue[0].pop_front();
		while (!queue[1].empty() && queue[1].front()<i-k+1) queue[1].pop_front();
		queue[0].push_back(i);
		queue[1].push_back(i);
		ans[0].push_back(a[queue[0].front()]);
		ans[1].push_back(a[queue[1].front()]);
	}
	vector<int>::iterator iter;
	for (iter=ans[1].begin()+k-1; iter!=ans[1].end(); ++iter) cout<<*iter<<' ';
	cout<<endl;
	for (iter=ans[0].begin()+k-1; iter!=ans[0].end(); ++iter) cout<<*iter<<' ';
	cout<<endl;
	return 0;
}
