#include <cstdio>
#include <cassert>
#include <queue>
#include <algorithm>

const int maxn=2000;

std::priority_queue<int> heap;

int pool[2][maxn];
int *a=pool[0],*b=pool[1];

int main() {
	int tcase,n,m;
	register int i,j;
	std::scanf("%d",&tcase);
	while (tcase--) {
		std::scanf("%d%d",&m,&n);
		for (--m,i=0; i<n; ++i) std::scanf("%d",&a[i]);
		std::sort(a,a+n);
		while (m--) {
			for (i=0; i<n; ++i) scanf("%d",&b[i]);
			std::sort(b,b+n);
			for (i=0; i<n; ++i) heap.push(a[i]+b[0]);
			for (i=1; i<n; ++i) {
				for (j=0; j<n; ++j) {
					if (b[i]+a[j] > heap.top()) break;
					heap.pop();
					heap.push(b[i]+a[j]);
				}
			}
			i=0;
			while (!heap.empty()) a[i++]=heap.top(),heap.pop();
			std::sort(a,a+n);
		}
		for (i=0; i<n; ++i) printf("%d ",a[i]);
		putchar('\n');
	}
	return 0;
}
