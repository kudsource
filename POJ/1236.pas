{$inline on}
const
  maxn=100;

var
  adj:array [0..maxn,0..maxn] of boolean;
  stack,low,dis,contract,indeg,outdeg:array [0..maxn] of longint;
  instack:array [0..maxn] of boolean;
  cnt,scc_cnt,tot,n,i,j,k,ans:longint;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

function max(p,q:longint):longint; inline;
begin
  if p>q then exit(p) else exit(q);
end;

procedure push(k:longint); inline;
begin
  inc(cnt);
  stack[cnt]:=k;
end;

function pop:longint; inline;
begin
  pop:=stack[cnt];
  dec(cnt);
end;

procedure dfs(i:longint);
var
  j:longint;

begin
  inc(tot);
  low[i]:=tot;
  dis[i]:=tot;
  push(i);
  instack[i]:=true;
  for j:=1 to n do
    if adj[i,j] then
      begin
        if dis[j]=0 then dfs(j);
        if instack[j] then low[i]:=min(low[i],low[j]);
      end;
  if dis[i]=low[i] then
    begin
      repeat
        j:=pop;
        instack[j]:=false;
        contract[j]:=i;
      until j=i;
    end;
end;

begin
  readln(n);
  for i:=1 to n do
    begin
      read(k);
      while k>0 do
        begin
          adj[i,k]:=true;
          read(k);
        end;
    end;
  cnt:=0;
  tot:=0;
  for i:=1 to n do if dis[i]=0 then dfs(i);
  for i:=1 to n do inc(scc_cnt,ord(contract[i]=i));
  for i:=1 to n do
    for j:=1 to n do
      adj[contract[i],contract[j]]:=adj[i,j] or adj[contract[i],contract[j]];
  for i:=1 to n do
    for j:=1 to n do
      if (contract[i]<>contract[j]) and (adj[contract[i],contract[j]]) then
        begin
          inc(indeg[j]);
          inc(outdeg[i]);
          adj[contract[i],contract[j]]:=false;
        end;
  if scc_cnt=1 then
    begin
      writeln(1);
      writeln(0);
      halt;
    end;
  ans:=0;
  tot:=0;
  for i:=1 to n do
    if contract[i]=i then
      begin
        inc(ans,ord(indeg[i]=0));
        inc(tot,ord(outdeg[i]=0));
      end;
  tot:=max(ans,tot);
  if scc_cnt=1 then
    begin
      ans:=1;
      tot:=0;
    end;
  writeln(ans);
  writeln(tot);
end.
