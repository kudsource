const
	maxn=5010;

var
	n,i,j,p,q:longint;
	a,b:ansistring;
	f:array [0..1,0..maxn] of longint;
	
function max(p,q:longint):longint;
begin
	if p>q then exit(p) else exit(q);
end;
	
begin
	readln(n);
	readln(a);
	for i:=length(a) downto 1 do b:=b+a[i];
	p:=0;
	q:=1;
	for i:=1 to n do
		begin
			for j:=1 to n do 
				begin
					f[q,j]:=max(f[p,j],f[q,j-1]);
					if a[i]=b[j] then f[q,j]:=max(f[q,j],f[p,j-1]+1);
				end;
			p:=1-p;
			q:=1-q;
		end;
	writeln(n-f[p,n]);
end.