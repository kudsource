const
  maxn=200000;
  maxm=255;

var
  p,q:ansistring;
  sa,rank,tsa,trank,h,sum:array [0..maxn] of longint;
  n,i,tmp,ans:longint;

function max(p,q:longint):longint;
begin
  if p>q then exit(p) else exit(q);
end;

procedure suffix_array(var s:ansistring);
var
  m,i,j,p:longint;

begin
  m:=255;
  fillchar(sum,sizeof(sum),0);
  for i:=1 to n do
    begin
      trank[i]:=ord(s[i]);
      inc(sum[trank[i]]);
    end;
  for i:=2 to m do inc(sum[i],sum[i-1]);
  for i:=n downto 1 do
    begin
      sa[sum[trank[i]]]:=i;
      dec(sum[trank[i]]);
    end;
  rank[sa[1]]:=1;
  p:=1;
  for i:=2 to n do
    begin
      if trank[sa[i]]<>trank[sa[i-1]] then inc(p);
      rank[sa[i]]:=p;
    end;
  m:=p;
  j:=1;
  while m<n do
    begin
      move(rank,trank,sizeof(rank));
      fillchar(sum,sizeof(sum),0);
      p:=0;
      for i:=n-j+1 to n do
        begin
          inc(p);
          tsa[p]:=i;
        end;
      for i:=1 to n do
        if sa[i]>j then
          begin
            inc(p);
            tsa[p]:=sa[i]-j;
          end;
      for i:=1 to n do
        begin
          rank[i]:=trank[tsa[i]];
          inc(sum[rank[i]]);
        end;
      for i:=2 to m do inc(sum[i],sum[i-1]);
      for i:=n downto 1 do
        begin
          sa[sum[rank[i]]]:=tsa[i];
          dec(sum[rank[i]]);
        end;
      rank[sa[1]]:=1;
      p:=1;
      for i:=2 to n do
        begin
          if (trank[sa[i]]<>trank[sa[i-1]]) or (trank[sa[i]+j]<>trank[sa[i-1]+j]) then inc(p);
          rank[sa[i]]:=p;
        end;
      j:=j shl 1;
      m:=p;
    end;
  h[1]:=0;
  p:=0;
  for i:=1 to n do
    begin
      if rank[i]=1 then continue;
      j:=sa[rank[i]-1];
      while s[i+p]=s[j+p] do inc(p);
      h[rank[i]]:=p;
      if p>0 then dec(p);
    end;
end;

begin
  readln(p);
  readln(q);
  tmp:=length(p);
  p:=p+'$'+q+'!';
  n:=length(p);
  suffix_array(p);
  for i:=2 to n do
    if (sa[i]>tmp) xor (sa[i-1]>tmp) then ans:=max(ans,h[i]);
  writeln(ans);
end.