#include <cstdio>
#include <cstring>
#include <vector>
#include <stack>
#include <set>
#include <algorithm>

using namespace std;

const int maxn = 5010, maxm = maxn * maxn;

int n,m,x[maxm],y[maxm];

vector<int> epool[maxn];

int dfn[maxn],low[maxn],scc[maxn],cnt,tot;

stack<int> stk;
bool instk[maxn];

void tarjan(int i) {
	dfn[i] = low[i] = ++tot;
	stk.push(i), instk[i] = true;
	for (vector<int>::iterator iter = epool[i].begin(); iter != epool[i].end(); ++iter) {
		int j = *iter;
		if (!dfn[j]) tarjan(j);
		if (instk[j]) low[i] = min(low[i],low[j]);
	}
	if (low[i] == dfn[i]) {
		int j;
		++cnt;
		do {
			j = stk.top(), stk.pop(), instk[j] = false;
			scc[j] = cnt;
		} while (j != i);
	}
}

int main() {
	while (scanf("%d",&n) != EOF && n) {
		if (n == 0) return 0;
		scanf("%d",&m);
		for (int i=1; i<=n; ++i) epool[i].clear();
		for (int i=1; i<=m; ++i) {
			scanf("%d%d",x+i,y+i);
			epool[x[i]].push_back(y[i]);
		}
		memset(dfn,0,sizeof dfn);
		cnt = tot = 0;
		for (int i=1; i<=n; ++i) if (!dfn[i]) tarjan(i);
		int deg[maxn] = {0};
		for (int i=1; i<=m; ++i)
			if (scc[x[i]] != scc[y[i]]) ++deg[scc[x[i]]];
		set<int> ans;
		ans.clear();
		for (int i=1; i<=cnt; ++i) if (!deg[i]) ans.insert(i);
		for (int i=1; i<=n; ++i)
			if (ans.find(scc[i]) != ans.end()) printf("%d ",i);
		putchar('\n');
	}
}
