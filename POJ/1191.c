#include <stdio.h>
#include <math.h>

#define maxn 10
#define maxk 20
#define INT_MAX 100000000

int f[maxk][maxn][maxn][maxn][maxn];
int a[maxn][maxn],s[maxn][maxn];

int sum(int x1, int y1, int x2, int y2)
{
	int tmp=s[x2][y2]-s[x1-1][y2]-s[x2][y1-1]+s[x1-1][y1-1];
	return tmp*tmp;
}

int min(int p, int q)
{
	return p<q?p:q;
}

int dfs(int k, int x1, int y1, int x2, int y2)
/*
(x1,y1):upper-left
(x2,y2):bottom-right
*/
{	
	if (f[k][x1][y1][x2][y2]) return f[k][x1][y1][x2][y2];
	if (k==1) return f[k][x1][y1][x2][y2]=sum(x1,y1,x2,y2);
	int i,tmp=INT_MAX;
	for (i=x1; i<x2; i++)
	{
		tmp=min(tmp,dfs(k-1,x1,y1,i,y2)+sum(i+1,y1,x2,y2));
		tmp=min(tmp,dfs(k-1,i+1,y1,x2,y2)+sum(x1,y1,i,y2));
	}
	for (i=y1; i<y2; i++)
	{
		tmp=min(tmp,dfs(k-1,x1,y1,x2,i)+sum(x1,i+1,x2,y2));
		tmp=min(tmp,dfs(k-1,x1,i+1,x2,y2)+sum(x1,y1,x2,i));
	}
	return f[k][x1][y1][x2][y2]=tmp;
}

int main()
{
	int n;
	int i,j;
	scanf("%d",&n);
	for (i=1; i<=8; i++)
		for (j=1; j<=8; j++) 
		{
			scanf("%d",&(a[i][j]));
			s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+a[i][j];
		}
	double ans=(double)dfs(n,1,1,8,8)/(double)n - (double)sum(1,1,8,8)/(double)(n*n);
	printf("%.3f\n",sqrt(ans));
	return 0;
}