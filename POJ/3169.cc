#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>

using namespace std;

const int maxn=1010,maxm=100000,oo=1<<20;

int cnt=0,ehead[maxn],n;

struct
{
	int adj,next,cost;
} edge[maxm];

inline void init()
{
	memset(ehead,255,sizeof(ehead));
	cnt=0;
}

inline void insert_edge(int p, int q, int c)
{
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	edge[cnt].cost=c;
	ehead[p]=cnt++;
}

long long spfa()
{
	queue<int> q;
	bool inqueue[maxn]={false};
	long long dis[maxn];
	int time[maxn]={0};
	for (int i=1; i<=n; ++i) dis[i]=oo;
	q.push(1);
	dis[1]=0;
	inqueue[1]=true;
	while (!q.empty())
	{
		int i=q.front();
		q.pop();
		inqueue[i]=false;
		for (int p=ehead[i]; p!=-1; p=edge[p].next)
		{
			int j=edge[p].adj,w=edge[p].cost;
			if (dis[i]+w<dis[j])
			{
				dis[j]=dis[i]+w;
				if (++time[j]>n) return -1;
				if (!inqueue[j])
				{
					q.push(j);
					inqueue[j]=true;
				}
			}
		}
	}
	if (dis[n]==oo) return -2; else return dis[n];
}

int main()
{
	int ML,MD,a,b,d;
	scanf("%d%d%d",&n,&ML,&MD);
	init();
	while (ML--)
	{
		scanf("%d%d%d",&a,&b,&d);
		insert_edge(a,b,d);
	}
	while (MD--)
	{
		scanf("%d%d%d",&a,&b,&d);
		insert_edge(b,a,-d);
	}
	for (int i=1; i<=n; ++i)
	{
		insert_edge(i,1,0);
		insert_edge(i+1,i,0);
	}
	printf("%lld\n",spfa());
	return 0;
}
