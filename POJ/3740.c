#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define maxn 300

bool vis[maxn],ans;
int a[maxn][maxn],m,n;

void dfs(int now, int sum) {
	if (sum == n) {
		ans=true;
		return;
	}
	if (ans || now == m) return;
	int i,tmp=0;
	for (i=0; i<n; ++i)
		if (vis[i] && a[now][i]) {
		   tmp=0;
		   break;
		} else tmp+=a[now][i];
	dfs(now+1,sum);
	if (tmp) {
		for (i=0; i<n; ++i) if (a[now][i]) vis[i]=true;
		dfs(now+1,sum+tmp);
		for (i=0; i<n; ++i) if (a[now][i]) vis[i]=false;
	}
}

char *res[2]={"It is impossible","Yes, I found it"};

int main() {
	register int i,j;
	while (scanf("%d%d",&m,&n) != EOF) {
		for (i=0; i<m; ++i)
			for (j=0; j<n; ++j)
				scanf("%d",&a[i][j]);
		memset(vis,false,sizeof(vis));
		ans=false;
		dfs(0,0);
		puts(res[ans]);
	}
	return 0;
}
