#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn=100000+10;


struct node
{
	int l,r,sum;
	node *lson,*rson;
	inline int mid() { return (l+r)>>1; }
} pool[maxn*20],*root[maxn];

int cnt=0,hash[maxn],sorted[maxn],data[maxn];

namespace SegmentTree
{
	node* build(int l, int r)
	{
		node *k=&pool[cnt++];
		k->l=l,k->r=r,k->sum=0;
		if (l==r) return k;
		int m=k->mid();
		k->lson=build(l,m);
		k->rson=build(m+1,r);
		return k;
	}
	node* change(node *orig, int x)
	{
		node *k=&pool[cnt++];
		*k=*orig;
		k->sum++;
		if (orig->l==x && orig->r==x) return k;
		int m=orig->mid();
		if (x<=m) k->lson=change(orig->lson,x); else
			k->rson=change(orig->rson,x);
		return k;
	}
	int query(node *left, node *right, int k)
	{
		if (left->l==left->r) return left->l;
		int tmp=left->lson->sum-right->lson->sum;
		if (k<=tmp) return query(left->lson,right->lson,k); else
			return query(left->rson,right->rson,k-tmp);
	}
}

int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	int p,i;
	for (i=1; i<=n; ++i) scanf("%d",&data[i]),sorted[i]=data[i];
	sort(sorted+1,sorted+n+1);
	for (p=0,i=1; i<=n; ++i) hash[++p]=sorted[i];
	root[0]=SegmentTree::build(1,p);
	for (int i=1; i<=n; ++i)
	{
		int tmp=lower_bound(sorted+1,sorted+n+1,data[i])-sorted;
		root[i]=SegmentTree::change(root[i-1],tmp);
	}
	while (m--)
	{
		int l,r,k;
		scanf("%d%d%d",&l,&r,&k);
		printf("%d\n",hash[SegmentTree::query(root[r],root[l-1],k)]);
	}
	return 0;
}
