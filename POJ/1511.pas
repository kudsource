{$inline on}
{$M 16772212}
const
  maxn=1000012;

var
  edge:array [0..maxn] of record
                            adj,next:longint;
                            cost:int64;
                          end;
  ehead:array [0..maxn] of longint;

  data:array [0..maxn] of record
                            src,dest:longint;
                            cost:int64;
                          end;
  cnt,i,n,m,t:longint;
  ans:int64;

procedure insert_edge(p,q:longint; cost:int64); inline;
begin
  edge[cnt].adj:=q;
  edge[cnt].next:=ehead[p];
  edge[cnt].cost:=cost;
  ehead[p]:=cnt;
  inc(cnt);
end;

procedure init; inline;
begin
  fillchar(ehead,sizeof(ehead),255);
  fillchar(edge,sizeof(edge),0);
  cnt:=0;
end;

function spfa:int64;
var
  head,tail,i,j,p,w:longint;
  dis:array [0..maxn] of int64;
  queue:array [0..maxn] of longint;
  v:array [0..maxn] of boolean;

begin
  fillchar(dis,sizeof(dis),$7F);
  fillchar(v,sizeof(v),false);
  fillchar(queue,sizeof(queue),0);
  head:=0;
  tail:=1;
  queue[tail]:=1;
  v[1]:=true;
  dis[1]:=0;
  repeat
    head:=(head mod maxn)+1;
    i:=queue[head];
    v[i]:=false;
    p:=ehead[i];
    while p<>-1 do
      begin
        j:=edge[p].adj;
        w:=edge[p].cost;
        if dis[i]+w<dis[j] then
          begin
            dis[j]:=dis[i]+w;
            if not v[i] then
              begin
                tail:=(tail mod maxn)+1;
                queue[tail]:=j;
                v[j]:=true;
              end;
          end;
        p:=edge[p].next;
      end;
  until head>=tail;
  spfa:=0;
  for i:=1 to n do inc(spfa,dis[i]);
end;

begin
{$IFNDEF ONLINE_JUDGE}
  assign(input,'invite.in'); 
  assign(output,'invite.out');
  reset(input);
  rewrite(output);
{$ENDIF}
  readln(t);
  repeat
    dec(t);
    readln(n,m);
    init;
    fillchar(data,sizeof(data),0);
    for i:=1 to m do
      begin
        readln(data[i].src,data[i].dest,data[i].cost);
        insert_edge(data[i].src,data[i].dest,data[i].cost);
      end;
    ans:=spfa;
    init;
    for i:=1 to m do insert_edge(data[i].dest,data[i].src,data[i].cost);
    inc(ans,spfa);
    writeln(ans);
  until t=0;
{$IFNDEF ONLINE_JUDGE}
  close(input);
  close(output);
{$ENDIF}
end.
