#include <stdio.h>

int lowbit(int x) { return x & -x; }

int main() {
	int tcase,n,delta;
	scanf("%d",&tcase);
	while (tcase--) {
		scanf("%d",&n);
		delta=n-lowbit(n);
		n-=delta;
		printf("%d %d\n",1+delta,n*2-1+delta);
	}
	return 0;
}
