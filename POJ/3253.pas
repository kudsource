const
  maxn=20010;

var
  heap:array [0..maxn] of longint;
  n,m,i,tmp:longint;
  ans:int64;

procedure swap(var p,q:longint);
begin
  p:=p xor q;
  q:=p xor q;
  p:=p xor q;
end;

procedure up(i:longint);
begin
  if i=1 then exit;
  if heap[i]<heap[i shr 1] then
    begin
      swap(heap[i],heap[i shr 1]);
      up(i shr 1);
    end;
end;

procedure down(i:longint);
var
  j:longint;

begin
  j:=i shl 1;
  if j>n then exit;
  if (j+1<=n) and (heap[j+1]<heap[j]) then inc(j);
  if heap[i]>heap[j] then
    begin
      swap(heap[i],heap[j]);
      down(j);
    end;
end;

function delmin:longint;
begin
  delmin:=heap[1];
  heap[1]:=heap[n];
  dec(n);
  down(1);
end;

begin
  readln(m);
  n:=m;
  for i:=1 to m do read(heap[i]);
  for i:=m shr 1 downto 1 do down(i);
  for i:=2 to m do
    begin
      tmp:=delmin+delmin;
      inc(ans,tmp);
      inc(n);
      heap[n]:=tmp;
      up(n);
    end;
  writeln(ans);
end.