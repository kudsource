#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=100001;

struct
{
	int adj,next,w;
} edge[maxn*2];

struct
{
	int src,dest,w;
} data[maxn];

int father[maxn],size[maxn],son[maxn],top[maxn],w[maxn],dep[maxn];
int cnt,tot,ehead[maxn],tree[maxn];

inline void link(int p, int q, int w)
{
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	edge[cnt].w=w;
	ehead[p]=cnt++;
}

void dfs(int i)
{
	son[i]=0,size[i]=1;
	for (int p=ehead[i]; p!=-1; p=edge[p].next)
	{
		int j=edge[p].adj;
		if (!dep[j]) 
		{
			father[j]=i;
			dep[j]=dep[i]+1;
			dfs(j);
			size[i]+=size[j];
			if (size[j]>size[son[i]]) son[i]=j;
		}
	}
}

void split(int i, int tp)
{
	top[i]=tp;
	w[i]=++tot;
	if (son[i]) split(son[i],top[i]);
	for (int p=ehead[i]; p!=-1; p=edge[p].next)
	{
		int j=edge[p].adj;
		if (j!=father[i] && j!=son[i]) split(j,j);
	}
}

namespace TreeArray
{
	inline int lowbit(int x) { return x & ( x ^ (x-1) ); }
	inline void change(int k, int c)
	{
		for (; k<=tot; k+=lowbit(k)) tree[k]+=c;
	}
	inline int query(int k)
	{
		int res=0;
		for (; k>0; k-=lowbit(k)) res+=tree[k];
		return res;
	}
}

int query(int p, int q)
{
	int res=0,t1=top[p],t2=top[q];
	while (t1!=t2)
	{
		if (dep[t1]<dep[t2]) swap(p,q),swap(t1,t2);
		res+=TreeArray::query(w[p])-TreeArray::query(w[t1]-1);
		p=father[t1],t1=top[p];
	}
	if (p==q) return res;
	if (dep[p]>dep[q]) swap(p,q);
	res+=TreeArray::query(w[q])-TreeArray::query(w[p]);
	return res;
}

int main()
{
	int n,q,s;
	memset(ehead,255,sizeof(ehead));
	scanf("%d%d%d",&n,&q,&s);
	for (int i=1; i<n; ++i)
	{
		scanf("%d%d%d",&data[i].src,&data[i].dest,&data[i].w);
		link(data[i].src,data[i].dest,data[i].w);
		link(data[i].dest,data[i].src,data[i].w);
	}
	dep[1]=1;
	dfs(1);
	tot=0;
	split(1,1);
	for (int i=1; i<n; ++i) 
	{
		if (dep[data[i].src]>dep[data[i].dest]) swap(data[i].src,data[i].dest);
		TreeArray::change(w[data[i].dest],data[i].w);
	}
	int order,u,c;
	while (q--)
	{
		scanf("%d",&order);
		switch (order)
		{
			case 0:
				scanf("%d",&u);
				printf("%d\n",query(s,u));
				s=u;
				break;
			case 1:
				scanf("%d%d",&u,&c);
				TreeArray::change(w[data[u].dest],c-data[u].w);
				data[u].w=c;
				break;
		}
	}
	return 0;
}
