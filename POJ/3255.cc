#pragma GCC optimize ("O2")

#include <cstdio>
#include <cstring>
#include <climits>
#include <queue>
#include <algorithm>

using std::scanf;
using std::printf;

const int maxn=5001,maxm=100001;

struct
{
	int adj,next,cost;
} edge[maxm<<1];

struct 
{
	int src,dest,cost;
} data[maxm];

int tot,cnt,ehead[maxn];

void init()
{
	std::memset(ehead,255,sizeof(ehead));
	cnt=0;
}

void insert_edge(int p, int q, int c)
{
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	edge[cnt].cost=c;
	ehead[p]=cnt++;
}

void spfa(int src, int dis[])
{
	std::queue<int> queue;
	bool inqueue[maxn]={false};
	queue.push(src);
	dis[src]=0;
	inqueue[src]=true;
	while (!queue.empty()) 
	{
		int i=queue.front();
		queue.pop();
		inqueue[i]=false;
		for (int p=ehead[i]; p!=-1; p=edge[p].next)
		{
			int j=edge[p].adj,w=edge[p].cost;
			if (w>0 && dis[i]+w<dis[j])
			{
				dis[j]=dis[i]+w;
				if (!inqueue[j])
				{
					queue.push(j);
					inqueue[j]=true;
				}
			}
		}
	}
}

int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	int x,y,c;
	init();
	for (int i=1; i<=m; ++i)
	{
		scanf("%d%d%d",&data[i].src,&data[i].dest,&data[i].cost);
		insert_edge(data[i].src,data[i].dest,data[i].cost);
		insert_edge(data[i].dest,data[i].src,data[i].cost);
	}
	int dis[maxn],rdis[maxn];
	memset(dis,127,sizeof(dis));
	memset(rdis,127,sizeof(rdis));
	spfa(1,dis);
	spfa(n,rdis);
	int ans=INT_MAX;
	for (int i=1; i<=n; ++i)
		for (int p=ehead[i]; p!=-1; p=edge[p].next)
		{
			int j=edge[p].adj,w=edge[p].cost;
			int tmp=dis[i]+w+rdis[j];
			if (tmp>dis[n]) ans=std::min(ans,tmp);
		}
	printf("%d\n",ans);
	return 0;
}
