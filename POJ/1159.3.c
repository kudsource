#include <stdio.h>
#include <string.h>

#define maxn 5001

char a[maxn];
short f[maxn][maxn];

int min(int p, int q) {
	return p<q?p:q;
}

int query(int l, int r) {
	if (f[l][r]) return f[l][r];
	if (l>=r) return f[l][r]=0;
	if (a[l]==a[r]) return f[l][r] = query(l+1, r-1); else
        return f[l][r] = min(query(l+1,r),query(l,r-1)) + 1;
}

int main() {
	int n;
	scanf("%d", &n);
	scanf("%s",a);
	printf("%d\n",query(0,n-1));
	return 0;
}