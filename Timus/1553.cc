#pragma comment(linker, "/STACK:16777216")

#include <cstdio>
#include <algorithm>

using std::scanf;
using std::printf;
using std::swap;

const int maxn=100000+10;

int w[maxn],top[maxn],size[maxn],son[maxn],father[maxn],dep[maxn];
int cnt=0,tot=0;

struct
{
	int adj,next;
} edge[maxn*2];

int ehead[maxn];

void link(int x, int y)
{
	edge[cnt].adj=y;
	edge[cnt].next=ehead[x];
	ehead[x]=cnt++;
}

void dfs(int i)
{
	size[i]=1;
	son[i]=0;
	for (int p=ehead[i]; p!=-1; p=edge[p].next)
	{
		int j=edge[p].adj;
		if (father[i]!=j) 
		{
			father[j]=i;
			dep[j]=dep[i]+1;
			dfs(j);
			size[i]+=size[j];
			if (size[son[i]]<size[j]) son[i]=j;
		}
	}
}

void split(int i, int tp)
{
	w[i]=++tot;
	top[i]=tp;
	if (son[i]) split(son[i],top[i]);
	for (int p=ehead[i]; p!=-1; p=edge[p].next)
	{
		int j=edge[p].adj;
		if (father[i]!=j && son[i]!=j) split(j,j);
	}
}

namespace Segtree
{
	int max[maxn*10];
	
	inline void update(int idx)
	{
		max[idx]=std::max(max[idx<<1],max[idx<<1|1]);
	}
	
	void insert(int l, int r, int pos, int num, int idx)
	{
		if (l==r)
		{
			max[idx]+=num;
			return;
		}
		int mid=(l+r)>>1;
		if (pos<=mid) insert(l,mid,pos,num,idx<<1); else insert(mid+1,r,pos,num,idx<<1|1);
		update(idx);
	}
	
	int query(int l, int r, int p, int q, int idx)
	{
		if (p<=l && r<=q) return max[idx];
		int mid=(l+r)>>1,res=0;
		if (p<=mid) res=std::max(res,query(l,mid,p,q,idx<<1));
		if (q>mid) res=std::max(res,query(mid+1,r,p,q,idx<<1|1));
		return res;
	}
}

int solve(int u, int v)
{
	int t1=top[u],t2=top[v],res=0;
	while (t1!=t2)
	{
		if (dep[t1]<dep[t2]) swap(t1,t2),swap(u,v);
		res=std::max(res,Segtree::query(1,tot,w[t1],w[u],1));
		u=father[t1];
		t1=top[u];
	}
	if (dep[u]>dep[v]) swap(u,v);
	return std::max(res,Segtree::query(1,tot,w[u],w[v],1));
}

int main()
{
	int n,a,b;
	scanf("%d",&n);
	for (int i=1; i<=n; ++i) ehead[i]=-1;
	for (int i=1; i<n; ++i)
	{
		scanf("%d%d",&a,&b);
		link(a,b);
		link(b,a);
	}
	dep[1]=1;
	dfs(1);
	split(1,1);
	int q;
	scanf("%d",&q);
	char order;
	int u,v;
	while (q--)
	{
		scanf(" %c %d %d",&order,&u,&v);
		switch (order)
		{
			case 'I':
				Segtree::insert(1,tot,w[u],v,1);
				break;
			case 'G':
				printf("%d\n",solve(u,v));
				break;
		}
	}
	return 0;
}
